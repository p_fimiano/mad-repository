package it.polito.mad.madapplication.ui.editprofile.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.editprofile.EditProfileFragment

@Subcomponent
interface EditProfileComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): EditProfileComponent

    }

    fun inject(fragment: EditProfileFragment)

}