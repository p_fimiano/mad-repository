package it.polito.mad.madapplication.ui.login.di

import dagger.Module
import dagger.Subcomponent
import it.polito.mad.madapplication.ui.login.LoginActivity
import it.polito.mad.madapplication.ui.login.LoginFragment
import it.polito.mad.madapplication.ui.signin.di.SignInComponent

@Subcomponent(
    modules = [
        LoginModule::class,
        LoginSubcomponentModule::class
    ]
)
interface LoginComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): LoginComponent

    }

    fun signInComponent(): SignInComponent.Factory

    fun inject(fragment: LoginFragment)

    fun inject(activity: LoginActivity)

}

@Module(
    subcomponents = [
        SignInComponent::class
    ]
)
object LoginSubcomponentModule