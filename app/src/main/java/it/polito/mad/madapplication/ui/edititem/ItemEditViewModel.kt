package it.polito.mad.madapplication.ui.edititem

import android.content.Context
import android.net.Uri
import androidx.lifecycle.*
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.gms.maps.model.LatLng
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.getUri
import it.polito.mad.madapplication.util.toLocation
import it.polito.mad.madapplication.worker.UploadWorker
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.*
import javax.inject.Inject


class ItemEditViewModel(
    private val userRepository: UserRepository,
    private val itemRepository: ItemRepository,
    private val ioDispatcher: CoroutineDispatcher,
    private val context: Context,
    private val item: Item
) : ViewModel() {

    val logoutEvent: LiveData<Event<Unit>> = MediatorLiveData<Event<Unit>>().also { result ->
        result.addSource(userRepository.getCurrentLiveData()) {
            if (it?.uid != item.userId) {
                result.value = Event(Unit)
            }
        }
    }

    private val workManager: WorkManager = WorkManager.getInstance(context)

    val title = MutableLiveData<String>()

    val description = MutableLiveData<String>()

    val category = MutableLiveData<String>()

    private val _expiryDate = MutableLiveData<Long>()
    val expiryDate: LiveData<Long> = _expiryDate

    private val _location = MutableLiveData<LatLng?>()
    val location: LiveData<LatLng?> = _location

    val price = MutableLiveData<String>()

    private val _interestedUserEvent = MutableLiveData<Event<List<User>>>()
    val interestedUserEvent: LiveData<Event<List<User>>> = _interestedUserEvent

    private val _boughtBy = MutableLiveData<String?>()
    val boughtBy: LiveData<String?> = _boughtBy

    private val _snackbarTextEvent = MutableLiveData<Event<Int>>()
    val snackbarTextEvent: LiveData<Event<Int>> = _snackbarTextEvent

    private val _notAllFilled = MutableLiveData<Boolean>()
    val notAllFilled: LiveData<Boolean> = _notAllFilled

    private val _onBackPressedEvent = MutableLiveData<Event<Boolean>>()
    val onBackPressedEvent: LiveData<Event<Boolean>> = _onBackPressedEvent

    private val _takePhotoEvent = MutableLiveData<Event<Uri>>()
    val takePhotoEvent: LiveData<Event<Uri>> = _takePhotoEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    private val _saveEvent = MutableLiveData<Event<String>>()
    val saveEvent: LiveData<Event<String>> = _saveEvent

    private val _currentPhotoUri = MutableLiveData<Uri?>()
    val currentPhotoUri: LiveData<Uri?> = _currentPhotoUri

    private var currentPhotoFile: File? = null

    val isNewItem: Boolean = item.id == null

    init {

        title.value = item.title
        description.value = item.description
        category.value = item.category
        _expiryDate.value = item.expiryDate
        _location.value = item.location?.toLatLng()
        price.value = item.price?.toString()
        _currentPhotoUri.value = item.photo?.toUri()
        _dataLoading.value = false
        _boughtBy.value = item.boughtBy

    }

    fun delete() {
        if (_dataLoading.value == true) {
            return
        }

        if (item.id == null) {
            _onBackPressedEvent.value = Event(false)
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val result = itemRepository.deleteById(item.id)
            if (result is Result.Success) {
                _onBackPressedEvent.value = Event(false)
            } else {
                notifyError()
            }
            _dataLoading.value = false
        }

    }

    private fun notifyError() {
        _snackbarTextEvent.value = Event(R.string.something_went_wrong)
    }

    fun loadInterested() {
        if (_dataLoading.value == true) {
            return
        }

        _interestedUserEvent.value?.peekContent()?.also {
            _interestedUserEvent.value = Event(it)
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val result = userRepository.getAllByIds(item.userIds)
            if (result is Result.Success) {
                _interestedUserEvent.value = Event(result.data)
            } else {
                notifyError()
            }

            _dataLoading.value = false
        }

    }

    fun sellTo(uid: String) {
        if (_dataLoading.value == true) {
            return
        }

        if (item.id == null) {
            // TODO: Item doesn't exist
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val result = itemRepository.sellToById(item.id, uid)
            if (result is Result.Success) {
                _boughtBy.value = uid
            } else {
                notifyError()
            }

            _dataLoading.value = false
        }

    }

    fun save() {

        if (_dataLoading.value == true) {
            return
        }

        _notAllFilled.value = false

        val currentTitle = title.value
        val currentDescription = description.value
        val currentCategory = category.value
        val currentPrice = price.value?.toFloatOrNull()
        val currentLocation = location.value?.toLocation()
        val currentExpiryDate = expiryDate.value
        val currentBuyer = _boughtBy.value

        if (currentTitle.isNullOrBlank() || currentDescription.isNullOrBlank() ||
            currentCategory.isNullOrBlank() || currentPrice == null || currentExpiryDate == null ||
            currentLocation == null
        ) {
            _notAllFilled.value = true
            return
        }

        val currentItem = item.copy(
            title = currentTitle,
            description = currentDescription,
            category = currentCategory,
            price = currentPrice,
            location = currentLocation,
            expiryDate = currentExpiryDate,
            boughtBy = currentBuyer
        )

        val currentPhotoUri = _currentPhotoUri.value

        _dataLoading.value = true
        viewModelScope.launch {

            val result = itemRepository.save(currentItem)
            if (result is Result.Success) {

                _saveEvent.value = Event(result.data)

                if (currentPhotoUri != null && currentPhotoUri != item.photo?.toUri()) {
                    uploadPhoto(result.data, currentPhotoUri)
                }

            } else {
                notifyError()
            }

            _dataLoading.value = false

        }
    }

    private fun uploadPhoto(itemId: String, uri: Uri) {

        val fileId = "$itemId-${System.currentTimeMillis()}"

        val inputData = UploadWorker.Builder(fileId, uri.toString())
            .updateField("items", itemId, Item::photo::name.get())
            .cleanRemoteFile(item.photo?.id)
            .build()

        val request = OneTimeWorkRequestBuilder<UploadWorker>()
            .setInputData(inputData)
            .build()

        workManager.enqueueUniqueWork(itemId, ExistingWorkPolicy.REPLACE, request)

    }

    fun onDateSet(year: Int, month: Int, dayOfMonth: Int) {

        val c = Calendar.getInstance()
        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, month)
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        _expiryDate.value = c.timeInMillis

    }

    fun takePhoto() {

        _takePhotoEvent.value?.peekContent()?.also {
            _takePhotoEvent.value = Event(it)
            return
        }

        if (_dataLoading.value == true) {
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            createPhotoUri()?.also {
                _takePhotoEvent.value = Event(it)
            }

            _dataLoading.value = false
        }

    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun createPhotoUri() = withContext(ioDispatcher) {
        try {

            var photoFile = currentPhotoFile
            if (photoFile == null) {
                val fileDir = context.cacheDir
                photoFile = File.createTempFile(
                    "PNG_",
                    ".png",
                    fileDir
                ).also {
                    currentPhotoFile = it
                }
            }

            context.getUri(photoFile!!)

        } catch (e: IOException) {
            Timber.e(e)
            null
        }
    }

    fun onBackPressed() {

        val currentTitle = title.value
        val currentDescription = description.value
        val currentCategory = category.value
        val currentPrice = price.value?.toFloatOrNull()
        val currentLocation = location.value?.toLocation()
        val currentExpiryDate = expiryDate.value
        val currentPhotoUri = _currentPhotoUri.value

        val currentItem = item.copy(
            title = currentTitle,
            description = currentDescription,
            category = currentCategory,
            price = currentPrice,
            location = currentLocation,
            expiryDate = currentExpiryDate
        )

        if (currentItem != item || item.photo?.toUri() != currentPhotoUri) {
            _onBackPressedEvent.value = Event(true)
            return
        }

        _onBackPressedEvent.value = Event(false)

    }

    fun loadPhoto(uri: Uri?) {
        _currentPhotoUri.value = uri ?: _takePhotoEvent.value?.peekContent()
                ?: throw RuntimeException()
    }

    fun newLocation(latLng: LatLng) {
        _location.value = latLng
    }

    class Factory @Inject constructor(
        private val userRepository: UserRepository,
        private val itemRepository: ItemRepository,
        private val ioDispatcher: CoroutineDispatcher,
        private val context: Context
    ) {
        fun create(item: Item): ItemEditViewModel {
            return ItemEditViewModel(userRepository, itemRepository, ioDispatcher, context, item)
        }
    }

}