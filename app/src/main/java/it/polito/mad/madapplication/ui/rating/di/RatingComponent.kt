package it.polito.mad.madapplication.ui.rating.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.rating.RatingFragment

@Subcomponent(
    modules = [
        RatingModule::class
    ]
)
interface RatingComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): RatingComponent
    }

    fun inject(fragment: RatingFragment)

}