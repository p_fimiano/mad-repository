package it.polito.mad.madapplication.data.source

import android.annotation.SuppressLint
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.util.await
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GeoRepositoryImpl @Inject constructor(
    private val fusedLocationClient: FusedLocationProviderClient,
    private val ioDispatcher: CoroutineDispatcher,
    private val geoApiContext: GeoApiContext
) : GeoRepository {

    @SuppressLint("MissingPermission")
    override suspend fun getLastLocation() = withContext(ioDispatcher) {
        try {

            val location = fusedLocationClient.lastLocation
                .await()

            Result.Success(LatLng(location.latitude, location.longitude))

        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun getDirections(from: LatLng, to: LatLng) = withContext(ioDispatcher) {

        try {

            val result = DirectionsApi.newRequest(geoApiContext)
                .origin(com.google.maps.model.LatLng(from.latitude, from.longitude))
                .destination(com.google.maps.model.LatLng(to.latitude, to.longitude))
                .await()

            Result.Success(result)

        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }

    }

}