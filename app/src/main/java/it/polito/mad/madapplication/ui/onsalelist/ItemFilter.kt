package it.polito.mad.madapplication.ui.onsalelist

import android.widget.Filter
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.ui.ItemAdapter

class ItemFilter(val adapter: ItemAdapter) : Filter() {

    private var currentConstraint: CharSequence? = null

    @Volatile
    private var currentList: List<Item> = emptyList()

    fun submitList(list: List<Item>?) {
        currentList = list ?: emptyList()
        filter(currentConstraint)
    }

    fun perform(constraint: CharSequence?) {
        currentConstraint = constraint
        filter(constraint)
    }

    override fun performFiltering(constraint: CharSequence?): FilterResults {

        val results = if (!constraint.isNullOrBlank()) {
            currentList.filter { it.matchConstraint(constraint) }
        } else {
            currentList
        }

        return FilterResults().apply {
            count = results.size
            values = results
        }

    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults) {
        @Suppress("UNCHECKED_CAST")
        adapter.submitList(results.values as List<Item>)
    }

}

private fun Item.matchConstraint(constraint: CharSequence): Boolean {
    return title?.contains(constraint, ignoreCase = true) == true ||
            description?.contains(constraint, ignoreCase = true) == true ||
            category?.contains(constraint, ignoreCase = true) == true
}