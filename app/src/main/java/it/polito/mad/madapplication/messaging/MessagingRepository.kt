package it.polito.mad.madapplication.messaging

interface MessagingRepository {

    suspend fun subscribe(topic: String)

    suspend fun unsubscribe(topic: String)

}