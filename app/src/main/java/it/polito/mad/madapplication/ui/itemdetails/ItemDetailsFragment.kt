package it.polito.mad.madapplication.ui.itemdetails

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentItemDetailsBinding
import it.polito.mad.madapplication.ui.main.MAP_LOCATION_ZOOM
import it.polito.mad.madapplication.util.getAddress
import it.polito.mad.madapplication.util.load
import it.polito.mad.madapplication.util.observeEvent
import it.polito.mad.madapplication.util.openAlertDialog
import kotlinx.android.synthetic.main.fragment_item_details.*
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import java.text.SimpleDateFormat
import javax.inject.Inject


class ItemDetailsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ItemDetailsViewModel.Factory

    private val viewModel by viewModels<ItemDetailsViewModel> {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return viewModelFactory.create(args.itemId) as T
            }
        }
    }

    private val args by navArgs<ItemDetailsFragmentArgs>()

    private lateinit var viewDataBinding: FragmentItemDetailsBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.itemDetailsComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_item_details, container, false)
        viewDataBinding = FragmentItemDetailsBinding.bind(root).also {
            it.dateFormat = SimpleDateFormat.getDateInstance()
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        val supportMapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            val googleMap: GoogleMap = supportMapFragment.awaitMap()
            onMapReady(googleMap)
        }

        enable_map_switch?.setOnCheckedChangeListener { _, isChecked ->
            map.isVisible = isChecked
        }

        setupNavigation()
        setupDialogs()
        setupData()

    }

    private fun setupData() {
        viewModel.item.observe(viewLifecycleOwner) { item ->

            item_image_details.load(item?.photo?.toUri()) {

                it.placeholder(R.mipmap.ic_launcher_round)
                    .apply(RequestOptions.circleCropTransform())

            }

        }
    }

    private fun setupNavigation() {
        viewModel.editItemEvent.observeEvent(viewLifecycleOwner) {
            val action = ItemDetailsFragmentDirections
                .openItemEditFragment(it)
            findNavController().navigate(action)
        }
    }

    private fun setupDialogs() {

        viewModel.loginErrorEvent.observeEvent(viewLifecycleOwner) {
            openAlertDialog {

                setMessage(R.string.not_authorized)
                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }
        }

        viewModel.showUsersEvent.observeEvent(viewLifecycleOwner) {

            openAlertDialog {

                val users = it.map { it.getFullName() ?: it.email }.toTypedArray()
                setItems(users) { dialog, which ->
                    val action = ItemDetailsFragmentDirections
                        .openShowProfileFragment(it[which].uid)
                    findNavController().navigate(action)
                    dialog.dismiss()
                }

                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.edit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit -> {
                viewModel.editItem()
                return true
            }
        }
        return false
    }

    private suspend fun onMapReady(googleMap: GoogleMap) {

        with(googleMap) {
            uiSettings.setAllGesturesEnabled(false)
            setOnMapClickListener {
                openMapsFragment()
            }
        }

        viewModel.item.asFlow().collect { item ->

            googleMap.clear()

            item?.location?.also {

                val location = it.toLatLng()

                item_location_details.text = try {
                    @Suppress("BlockingMethodInNonBlockingContext")
                    location.getAddress(requireContext())?.locality
                } catch (e: Exception) {
                    Timber.e(e)
                    null
                }

                with(googleMap) {
                    addMarker { position(location) }
                    animateCamera(CameraUpdateFactory.newLatLngZoom(location, MAP_LOCATION_ZOOM))
                }

            }

        }

    }

    private fun openMapsFragment() {
        val location = viewModel.item.value?.location ?: return
        val action = ItemDetailsFragmentDirections
            .openMapsFragment(location.toLatLng())
        findNavController().navigate(action)
    }

}
