package it.polito.mad.madapplication.ui.boughtitemslist.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.boughtitemslist.BoughtItemsListFragment

@Subcomponent(
    modules = [
        BoughtItemsListModule::class
    ]
)
interface BoughtItemsListComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): BoughtItemsListComponent

    }

    fun inject(fragment: BoughtItemsListFragment)

}