package it.polito.mad.madapplication.data

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
) : Parcelable {

    fun toLatLng(): LatLng {
        return LatLng(latitude, longitude)
    }

}