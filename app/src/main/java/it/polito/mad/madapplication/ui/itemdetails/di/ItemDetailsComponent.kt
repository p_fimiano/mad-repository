package it.polito.mad.madapplication.ui.itemdetails.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.itemdetails.ItemDetailsFragment

@Subcomponent
interface ItemDetailsComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): ItemDetailsComponent

    }

    fun inject(fragment: ItemDetailsFragment)

}