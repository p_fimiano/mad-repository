@file:Suppress("unused", "ClassName")

package it.polito.mad.madapplication.util

import android.content.Context
import java.util.*
import kotlin.math.roundToInt

fun <T> lazyFast(initializer: () -> T): Lazy<T> = lazy(LazyThreadSafetyMode.NONE, initializer)

fun String.takeIfNotEmpty(): String? {
    return takeIf { isNotEmpty() }
}

fun String.takeIfNotBlank(): String? {
    return takeIf { isNotBlank() }
}

fun Int.dpToPx(context: Context): Int {
    return (context.resources.displayMetrics.density * this).roundToInt()
}

fun Long.toDate() = Date(this)

inline fun <T> Iterable<T>.averageBy(selector: (T) -> Float): Float {
    var sum = 0.0F
    var size = 0
    for (value in this) {
        sum += selector(value)
        size++
    }
    if (size == 0) {
        return 0.0F
    }
    return sum / size
}
