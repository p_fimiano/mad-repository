package it.polito.mad.madapplication.ui.editprofile

import android.content.Context
import android.net.Uri
import android.util.Patterns
import androidx.lifecycle.*
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.getUri
import it.polito.mad.madapplication.util.toLocation
import it.polito.mad.madapplication.worker.UploadWorker
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.IOException
import javax.inject.Inject

class EditProfileViewModel(
    private val userRepository: UserRepository,
    private val ioDispatcher: CoroutineDispatcher,
    private val context: Context,
    private val user: User
) : ViewModel() {

    class Factory @Inject constructor(
        private val userRepository: UserRepository,
        private val ioDispatcher: CoroutineDispatcher,
        private val context: Context
    ) {
        fun create(user: User): EditProfileViewModel {
            return EditProfileViewModel(userRepository, ioDispatcher, context, user)
        }
    }

    private val workManager: WorkManager = WorkManager.getInstance(context)

    val loginEvent: LiveData<Event<Unit>?> = userRepository.getCurrentLiveData().map {
        if (it?.uid != user.uid) Event(Unit) else null
    }

    // Two ways data binding
    val email = MutableLiveData<String>()

    val name = MutableLiveData<String>()

    val surname = MutableLiveData<String>()

    val nickname = MutableLiveData<String>()

    private val _location = MutableLiveData<LatLng?>()
    val location: LiveData<LatLng?> = _location

    private val _loginRequiredError = MutableLiveData<Event<Unit>>()
    val loginRequiredError: LiveData<Event<Unit>> = _loginRequiredError

    private val _emailErrorEvent = MutableLiveData<Event<Int>>()
    val emailErrorEvent: LiveData<Event<Int>> = _emailErrorEvent

    private val _snackbarErrorText = MutableLiveData<Event<Int>>()
    val snackbarErrorText: LiveData<Event<Int>> = _snackbarErrorText

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    private val _saveEvent = MutableLiveData<Event<Unit>>()
    val saveEvent: LiveData<Event<Unit>> = _saveEvent

    private val _takePhotoEvent = MutableLiveData<Event<Uri>>()
    val takePhotoEvent: LiveData<Event<Uri>> = _takePhotoEvent

    private val _currentPhoto = MutableLiveData<Uri?>()
    val currentPhoto: LiveData<Uri?> = _currentPhoto

    private var currentPhotoFile: File? = null

    init {

        name.value = user.name
        surname.value = user.surname
        nickname.value = user.nickname
        _location.value = user.location?.toLatLng()
        email.value = user.email
        _currentPhoto.value = user.photo?.toUri()
        _dataLoading.value = false

    }

    fun takePhoto() {

        _takePhotoEvent.value?.peekContent()?.also { photoUri ->
            _takePhotoEvent.value = Event(photoUri)
            return
        }

        if (_dataLoading.value == true) {
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            createPhotoUri()?.also {
                _takePhotoEvent.value = Event(it)
            }

            _dataLoading.value = false
        }

    }

    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun createPhotoUri() = withContext(ioDispatcher) {
        try {

            var photoFile = currentPhotoFile
            if (photoFile == null) {
                val fileDir = context.cacheDir
                photoFile = File.createTempFile(
                    "PNG_",
                    ".png",
                    fileDir
                ).also {
                    currentPhotoFile = it
                }
            }

            context.getUri(photoFile!!)

        } catch (e: IOException) {
            Timber.e(e)
            null
        }
    }

    fun loadPhoto(uri: Uri?) {
        _currentPhoto.value = uri ?: _takePhotoEvent.value?.peekContent()
                ?: throw RuntimeException()
    }

    fun save() {

        if (_dataLoading.value == true) {
            return
        }

        val currentEmail = email.value
        if (currentEmail == null || !Patterns.EMAIL_ADDRESS.matcher(currentEmail).matches()) {
            _emailErrorEvent.value = Event(R.string.email_error)
            return
        }

        _emailErrorEvent.value = Event(0)

        val currentPhotoUri = _currentPhoto.value

        val currentName = name.value
        val currentSurname = surname.value
        val currentNickname = nickname.value
        val currentLocation = location.value?.toLocation()

        val newUser = user.copy(
            name = currentName,
            surname = currentSurname,
            nickname = currentNickname,
            location = currentLocation
        )

        _dataLoading.value = true
        viewModelScope.launch {

            val result = userRepository.saveCurrent(newUser)
            if (result is Result.Success) {

                _saveEvent.value = Event(Unit)

                if (currentPhotoUri != null && currentPhotoUri != user.photo?.toUri()) {
                    uploadPhoto(currentPhotoUri)
                }

                _dataLoading.value = false
            } else {
                onSaveFailed((result as? Result.Error)?.exception)
            }

        }

    }

    private fun onSaveFailed(exception: Exception?) {
        if (exception is FirebaseAuthRecentLoginRequiredException) {
            _loginRequiredError.value = Event(Unit)
        } else {
            _snackbarErrorText.value = Event(R.string.something_went_wrong)
        }
        _dataLoading.value = false
    }

    private fun uploadPhoto(uri: Uri) {

        val fileId = "${user.uid}-${System.currentTimeMillis()}"

        val uploadData = UploadWorker.Builder(fileId, uri.toString())
            .updateField("users", user.uid, User::photo::name.get())
            .cleanRemoteFile(user.photo?.id)
            .build()

        val uploadRequest = OneTimeWorkRequestBuilder<UploadWorker>()
            .setInputData(uploadData)
            .build()

        workManager.enqueueUniqueWork(user.uid, ExistingWorkPolicy.REPLACE, uploadRequest)

    }

    fun newLocation(latLng: LatLng) {
        _location.value = latLng
    }

}