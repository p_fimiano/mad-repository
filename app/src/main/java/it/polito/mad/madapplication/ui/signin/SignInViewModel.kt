package it.polito.mad.madapplication.ui.signin

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignInViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val email = MutableLiveData<String>()

    val password1 = MutableLiveData<String>()

    val password2 = MutableLiveData<String>()

    private val _signInErrorEvent = MutableLiveData<Event<Int>>()
    val signInErrorEvent: LiveData<Event<Int>> = _signInErrorEvent

    private val _signInEvent = MutableLiveData<Event<Unit>>()
    val signInEvent: LiveData<Event<Unit>> = _signInEvent

    private val _emailErrorEvent = MutableLiveData<Event<Int>>()
    val emailErrorEvent: LiveData<Event<Int>> = _emailErrorEvent

    private val _passwordErrorEvent = MutableLiveData<Event<Int>>()
    val passwordErrorEvent: LiveData<Event<Int>> = _passwordErrorEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    fun signIn() {
        if (_dataLoading.value == true) {
            return
        }

        _emailErrorEvent.value = Event(0)
        _passwordErrorEvent.value = Event(0)

        val currentEmail = email.value
        if (currentEmail == null || !Patterns.EMAIL_ADDRESS.matcher(currentEmail).matches()) {
            _emailErrorEvent.value =
                Event(R.string.email_error)
            return
        }

        val currentPassword1 = password1.value
        val currentPassword2 = password2.value

        password1.value = null
        password2.value = null

        if (currentPassword1.isNullOrBlank() || currentPassword1 != currentPassword2) {
            _passwordErrorEvent.value =
                Event(R.string.password_error)
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val result = userRepository.signIn(currentEmail, currentPassword1)
            if (result is Result.Success) {
                _signInEvent.value = Event(Unit)
                _dataLoading.value = false
            } else {
                onSignInError((result as? Result.Error)?.exception)
            }

            _dataLoading.value = false
        }
    }

    private fun onSignInError(exception: Exception?) {
        if (exception is FirebaseAuthUserCollisionException) {
            _emailErrorEvent.value =
                Event(R.string.user_already_enrolled_error)
        }
        _signInErrorEvent.value =
            Event(R.string.sign_in_error)
        _dataLoading.value = false
    }

}