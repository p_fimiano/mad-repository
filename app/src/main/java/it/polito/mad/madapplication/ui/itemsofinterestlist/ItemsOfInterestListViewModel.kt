package it.polito.mad.madapplication.ui.itemsofinterestlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.ui.ItemBaseViewModel
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.switchMap
import javax.inject.Inject

class ItemsOfInterestListViewModel @Inject constructor(
    userRepository: UserRepository,
    itemRepository: ItemRepository
) : ItemBaseViewModel() {

    val itemsOfInterest: LiveData<List<Item>?> =
        userRepository.getCurrentLiveData().switchMap { user ->
            user?.let { itemRepository.getLiveDataOfInterestByUser(it.uid) }
        }

    private val _loginErrorEvent = MutableLiveData<Event<Unit>>()
    val loginErrorEvent: LiveData<Event<Unit>> = _loginErrorEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

}