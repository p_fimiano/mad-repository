package it.polito.mad.madapplication.data.source

import androidx.lifecycle.LiveData
import it.polito.mad.madapplication.data.User

interface UserDataSource {

    fun getLiveDataById(uid: String): LiveData<User?>

    suspend fun getById(uid: String): User?

    suspend fun deleteById(uid: String)

    suspend fun save(user: User)

    suspend fun getAllByIds(uids: List<String>): List<User>?

}