package it.polito.mad.madapplication.ui.itemdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.combineLatestWith
import kotlinx.coroutines.launch
import javax.inject.Inject

class ItemDetailsViewModel(
    private val itemsRepository: ItemRepository,
    private val userRepository: UserRepository,
    itemId: String
) : ViewModel() {

    class Factory @Inject constructor(
        private val itemsRepository: ItemRepository,
        private val userRepository: UserRepository
    ) {
        fun create(itemId: String): ItemDetailsViewModel {
            return ItemDetailsViewModel(itemsRepository, userRepository, itemId)
        }
    }

    val item: LiveData<Item?> = itemsRepository.getLiveDataById(itemId)

    private val user: LiveData<User?> = userRepository.getCurrentLiveData()

    val likeItem: LiveData<Boolean> = item.combineLatestWith(user) { item, user ->
        item?.userIds?.contains(user?.uid) == true
    }

    val isFabVisible: LiveData<Boolean> = item.combineLatestWith(user) { item, user ->
        user != null && item != null && user.uid != item.userId && item.boughtBy == null
    }

    private val _showUsersEvent = MutableLiveData<Event<List<User>>>()
    val showUsersEvent: LiveData<Event<List<User>>> = _showUsersEvent

    private val _editItemEvent = MutableLiveData<Event<Item>>()
    val editItemEvent: LiveData<Event<Item>> = _editItemEvent

    private val _loginErrorEvent = MutableLiveData<Event<Unit>>()
    val loginErrorEvent: LiveData<Event<Unit>> = _loginErrorEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    fun editItem() {

        val currentItem = item.value ?: return

        val currentUser = user.value
        if (currentUser == null || currentUser.uid != currentItem.userId) {
            _loginErrorEvent.value = Event(Unit)
            return
        }

        _editItemEvent.value = Event(currentItem)

    }

    fun toggleLike() {
        if (_dataLoading.value == true) {
            return
        }

        val currentItem = item.value ?: return

        val currentUid = user.value?.uid
        if (currentUid == null) {
            _loginErrorEvent.value = Event(Unit)
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val uids = mutableListOf<String>().apply {
                addAll(currentItem.userIds)
                if (contains(currentUid)) {
                    remove(currentUid)
                } else {
                    add(currentUid)
                }
            }

            val result = itemsRepository.save(currentItem.copy(userIds = uids))
            if (result is Result.Error) {
                // TODO: Handle the error
            }

            _dataLoading.value = false
        }

    }

    fun openUsersDialog() {
        if (_dataLoading.value == true) {
            return
        }

        val currentItem = item.value ?: return
        if (currentItem.userIds.isEmpty()) {
            return
        }

        val currentUser = user.value ?: return
        if (currentUser.uid != currentItem.userId) {
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            // Fetch all users
            val result = userRepository.getAllByIds(currentItem.userIds)
            if (result is Result.Success && result.data.isNotEmpty()) {
                _showUsersEvent.value = Event(result.data)
            } else {
                // TODO: Handle the error
            }

            _dataLoading.value = false

        }

    }

}