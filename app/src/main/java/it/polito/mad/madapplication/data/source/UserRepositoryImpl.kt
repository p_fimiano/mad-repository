package it.polito.mad.madapplication.data.source

import androidx.lifecycle.LiveData
import it.polito.mad.madapplication.auth.AuthenticationManager
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.messaging.MessagingRepository
import it.polito.mad.madapplication.util.switchMap
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepositoryImpl @Inject constructor(
    private val authenticationManager: AuthenticationManager,
    private val messagingRepository: MessagingRepository,
    private val remoteDataSource: UserDataSource
) : UserRepository {

    private var _cachedLD: LiveData<User?>? = null

    override fun getCurrentLiveData() = _cachedLD ?: loadCachedLD()

    private fun loadCachedLD(): LiveData<User?> {
        return authenticationManager.getCurrentLiveData().switchMap { uid ->
            uid?.let { remoteDataSource.getLiveDataById(it) }
        }.also {
            _cachedLD = it
        }
    }


    override fun getLiveDataById(uuid: String) = remoteDataSource.getLiveDataById(uuid)

    override suspend fun getCurrent() = performOrError {
        val currentUid = authenticationManager.getCurrent()
        remoteDataSource.getById(currentUid)?.also {
            return@performOrError Result.Success(it)
        }
        Result.Error(Exception("User not found"))
    }

    override suspend fun getById(uuid: String) = performOrError {
        remoteDataSource.getById(uuid)?.also {
            return@performOrError Result.Success(it)
        }
        Result.Error(Exception("User not found"))
    }

    override suspend fun deleteCurrent() = performOrError {
        val currentUid = authenticationManager.getCurrent()
        coroutineScope {
            launch { remoteDataSource.deleteById(currentUid) }
            launch { authenticationManager.deleteCurrent() }
        }
        Result.Success(Unit)
    }

    override suspend fun saveCurrent(user: User) = performOrError {
        val currentUid = authenticationManager.getCurrent()
        coroutineScope {
            launch { remoteDataSource.save(user.copy(uid = currentUid)) }
            launch { authenticationManager.updateCurrentEmail(user.email) }
        }
        Result.Success(Unit)
    }

    override suspend fun login(email: String, password: String) = performOrError {
        // Do login
        authenticationManager.login(email, password)

        // Subscribe to notifications topic
        val currentUid = authenticationManager.getCurrent()
        messagingRepository.subscribe(currentUid)

        Result.Success(Unit)
    }

    override suspend fun signIn(email: String, password: String) = performOrError {
        authenticationManager.signIn(email, password).let { uid ->
            remoteDataSource.save(User(uid = uid, email = email))
            Result.Success(uid)
        }
    }

    override suspend fun logout() = performOrError {
        val currentUid = authenticationManager.getCurrent()

        coroutineScope {
            // Do logout
            launch { authenticationManager.logout() }
            // Unsubscribe from notifications topic
            launch { messagingRepository.unsubscribe(currentUid) }
        }

        Result.Success(Unit)
    }

    override suspend fun getAllByIds(uids: List<String>) = performOrError {
        remoteDataSource.getAllByIds(uids)?.also {
            return@performOrError Result.Success(it)
        }
        Result.Error(Exception())
    }

    private inline fun <T> performOrError(perform: () -> Result<T>): Result<T> {
        return try {
            perform.invoke()
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }

}