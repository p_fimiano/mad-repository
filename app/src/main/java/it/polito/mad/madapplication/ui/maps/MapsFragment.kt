package it.polito.mad.madapplication.ui.maps

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asFlow
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.PolyUtil
import com.google.maps.android.ktx.awaitMap
import com.google.maps.android.ktx.model.polylineOptions
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentMapsBinding
import it.polito.mad.madapplication.ui.main.REQUEST_ACCESS_LOCATION_PERMISSION
import it.polito.mad.madapplication.util.checkPermissions
import it.polito.mad.madapplication.util.dpToPx
import it.polito.mad.madapplication.util.observeEvent
import it.polito.mad.madapplication.util.showSnackbar
import kotlinx.android.synthetic.main.fragment_maps.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MapsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: MapsViewModel by viewModels { viewModelFactory }

    private val args: MapsFragmentArgs by navArgs()

    private lateinit var viewDataBinding: FragmentMapsBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.mapsComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_maps, container, false)
        viewDataBinding = FragmentMapsBinding.bind(root).also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val supportMapFragment: SupportMapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            val map = supportMapFragment.awaitMap()
            onMapReady(map)
        }

        setupBottomSheet()
        setupErrorEvent()

    }

    private fun setupBottomSheet() {
        val bottomSheetBehavior: BottomSheetBehavior<FrameLayout> =
            BottomSheetBehavior.from(bottom_view)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        toggle_btn.setOnClickListener {
            val state = bottomSheetBehavior.state
            bottomSheetBehavior.state = when (state) {
                BottomSheetBehavior.STATE_COLLAPSED -> BottomSheetBehavior.STATE_EXPANDED
                else -> BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (
            checkPermissions(
                arrayOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                REQUEST_ACCESS_LOCATION_PERMISSION
            )
        ) {
            viewModel.start(args.to)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_ACCESS_LOCATION_PERMISSION) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                viewModel.start(args.to)
            }
        }
    }

    private fun setupErrorEvent() {
        viewModel.errorEvent.observeEvent(viewLifecycleOwner) {
            view?.showSnackbar(getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG)
        }
    }

    private suspend fun onMapReady(googleMap: GoogleMap) {
        viewModel.route.asFlow().collect { route ->

            googleMap.clear()

            // Draw the polyline
            val points = withContext(Dispatchers.Default) {
                route.legs[0].steps.flatMap { step -> PolyUtil.decode(step.polyline.encodedPath) }
            }

            googleMap.addPolyline(polylineOptions { addAll(points) })

            // Zoom the area
            val northeast = LatLng(route.bounds.northeast.lat, route.bounds.northeast.lng)
            val southwest = LatLng(route.bounds.southwest.lat, route.bounds.southwest.lng)
            val bounds: LatLngBounds = LatLngBounds.Builder()
                .include(northeast)
                .include(southwest)
                .build()

            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                    bounds,
                    100.dpToPx(requireContext())
                )
            )

        }

    }
}