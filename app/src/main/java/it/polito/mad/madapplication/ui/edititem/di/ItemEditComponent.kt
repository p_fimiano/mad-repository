package it.polito.mad.madapplication.ui.edititem.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.edititem.ItemEditFragment

@Subcomponent
interface ItemEditComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): ItemEditComponent

    }

    fun inject(fragment: ItemEditFragment)

}