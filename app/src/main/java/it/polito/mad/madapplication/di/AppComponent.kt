package it.polito.mad.madapplication.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.ui.boughtitemslist.di.BoughtItemsListComponent
import it.polito.mad.madapplication.ui.edititem.di.ItemEditComponent
import it.polito.mad.madapplication.ui.editprofile.di.EditProfileComponent
import it.polito.mad.madapplication.ui.itemlist.di.ItemListComponent
import it.polito.mad.madapplication.ui.itemdetails.di.ItemDetailsComponent
import it.polito.mad.madapplication.ui.itemsofinterestlist.di.ItemsOfInterestListComponent
import it.polito.mad.madapplication.ui.login.di.LoginComponent
import it.polito.mad.madapplication.ui.main.di.MainComponent
import it.polito.mad.madapplication.ui.maps.di.MapsComponent
import it.polito.mad.madapplication.ui.onsalelist.di.OnSaleListComponent
import it.polito.mad.madapplication.ui.rating.di.RatingComponent
import it.polito.mad.madapplication.ui.showprofile.di.ShowProfileComponent
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AppModuleBinds::class,
        ViewModelFactoryModule::class,
        SubcomponentModule::class,
        WorkerFactoryModule::class,
        ViewModelFactoryProviderModule::class
    ]
)
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }

    fun showProfileComponent(): ShowProfileComponent.Factory

    fun itemListComponent(): ItemListComponent.Factory

    fun itemEditComponent(): ItemEditComponent.Factory

    fun itemDetailsComponent(): ItemDetailsComponent.Factory

    fun editProfileComponent(): EditProfileComponent.Factory

    fun loginComponent(): LoginComponent.Factory

    fun mainComponent(): MainComponent.Factory

    fun inject(application: MadApplication)

    fun itemsOfInterestListComponent(): ItemsOfInterestListComponent.Factory

    fun boughtItemsListComponent(): BoughtItemsListComponent.Factory

    fun mapsComponent(): MapsComponent.Factory

    fun ratingComponent(): RatingComponent.Factory

    fun onSaleListComponent(): OnSaleListComponent.Factory

}

@Module(
    subcomponents = [
        EditProfileComponent::class,
        ShowProfileComponent::class,
        LoginComponent::class,
        ItemListComponent::class,
        ItemEditComponent::class,
        ItemDetailsComponent::class,
        ItemsOfInterestListComponent::class,
        BoughtItemsListComponent::class,
        MapsComponent::class,
        RatingComponent::class,
        OnSaleListComponent::class
    ]
)
object SubcomponentModule