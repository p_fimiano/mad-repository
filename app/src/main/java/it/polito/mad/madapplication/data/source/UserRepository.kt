package it.polito.mad.madapplication.data.source

import androidx.lifecycle.LiveData
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.User

interface UserRepository {

    fun getCurrentLiveData(): LiveData<User?>

    fun getLiveDataById(uuid: String): LiveData<User?>

    suspend fun getCurrent(): Result<User>

    suspend fun getById(uuid: String): Result<User>

    suspend fun deleteCurrent(): Result<Unit>

    suspend fun saveCurrent(user: User): Result<Unit>

    suspend fun login(email: String, password: String): Result<Unit>

    suspend fun signIn(email: String, password: String): Result<String>

    suspend fun logout(): Result<Unit>

    suspend fun getAllByIds(uids: List<String>): Result<List<User>>

}