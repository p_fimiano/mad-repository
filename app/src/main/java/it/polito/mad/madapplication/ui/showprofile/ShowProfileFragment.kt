package it.polito.mad.madapplication.ui.showprofile

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.maps.android.ktx.awaitMap
import com.google.maps.android.ktx.model.markerOptions
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentShowProfileBinding
import it.polito.mad.madapplication.ui.main.MAP_LOCATION_ZOOM
import it.polito.mad.madapplication.util.*
import kotlinx.android.synthetic.main.fragment_show_profile.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class ShowProfileFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ShowProfileViewModel.Factory

    private val viewModel by viewModels<ShowProfileViewModel> {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return viewModelFactory.create(arg.userId) as T
            }
        }
    }

    private val arg by navArgs<ShowProfileFragmentArgs>()

    private lateinit var viewDataBinding: FragmentShowProfileBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.showProfileComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_show_profile, container, false)
        viewDataBinding = FragmentShowProfileBinding.bind(root).also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupNavigation()
        setupData()
        setupDialogs()
        setupMap()
    }

    private fun setupNavigation() {
        viewModel.editProfileEvent.observeEvent(viewLifecycleOwner) {
            val action = ShowProfileFragmentDirections
                .openEditProfileFragment(it)
            findNavController().navigate(action)
        }
    }

    private fun setupDialogs() {
        viewModel.loginErrorEvent.observeEvent(viewLifecycleOwner) {
            openAlertDialog {

                setMessage(R.string.not_logged_in_error)
                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }
        }
    }

    private fun setupData() {
        viewModel.user.observe(viewLifecycleOwner) { user ->

            user_image.load(user?.photo?.toUri()) {

                it.placeholder(R.mipmap.ic_launcher_round)
                    .apply(RequestOptions.circleCropTransform())

            }

        }

        viewModel.rating.observe(viewLifecycleOwner) {
            rating_bar.rating = it
        }
    }

    private fun setupMap() {
        val supportMapFragment: SupportMapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            val map: GoogleMap = supportMapFragment.awaitMap()
            onMapReady(map)
        }

        toggle_map.setOnCheckedChangeListener { _, isChecked ->
            map.goneIf(!isChecked)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.edit_menu, menu)
        val editMenuItem = menu.findItem(R.id.action_edit)
        viewModel.isEditable.observe(viewLifecycleOwner) {
            editMenuItem.isVisible = it
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit -> {
                viewModel.editProfile()
                return true
            }
        }
        return false
    }

    private suspend fun onMapReady(googleMap: GoogleMap) {

        googleMap.uiSettings.setAllGesturesEnabled(false)

        viewModel.user.asFlow().collect { user ->

            googleMap.clear()

            val location = user?.location
            if (location != null) {
                val point = location.toLatLng()
                @Suppress("BlockingMethodInNonBlockingContext")
                textView_location.text = point.getAddress(requireContext())?.locality
                googleMap.addMarker(markerOptions { position(point) })
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, MAP_LOCATION_ZOOM))
            } else {
                textView_location.text = null
            }

        }

    }

}
