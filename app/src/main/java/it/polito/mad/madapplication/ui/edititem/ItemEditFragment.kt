package it.polito.mad.madapplication.ui.edititem

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentItemEditBinding
import it.polito.mad.madapplication.ui.MySupportMapFragment
import it.polito.mad.madapplication.ui.main.MAP_LOCATION_ZOOM
import it.polito.mad.madapplication.ui.main.REQUEST_CAMERA_PERMISSION
import it.polito.mad.madapplication.ui.main.REQUEST_CHOOSE_PHOTO
import it.polito.mad.madapplication.ui.main.REQUEST_TAKE_PHOTO
import it.polito.mad.madapplication.util.*
import kotlinx.android.synthetic.main.fragment_item_edit.*
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ItemEditFragment : Fragment() {

    @Inject
    lateinit var itemEditViewModelFactory: ItemEditViewModel.Factory

    private val viewModel by viewModels<ItemEditViewModel> {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return itemEditViewModelFactory.create(args.item) as T
            }
        }
    }

    private val args by navArgs<ItemEditFragmentArgs>()

    private lateinit var viewDataBinding: FragmentItemEditBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.itemEditComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_item_edit, container, false)
        viewDataBinding = FragmentItemEditBinding.bind(root).also {
            it.dateFormat = SimpleDateFormat.getDateInstance()
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        val supportMapFragment = childFragmentManager
            .findFragmentById(R.id.map) as MySupportMapFragment
        supportMapFragment.listener = {
            scrollView.requestDisallowInterceptTouchEvent(true)
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            val googleMap: GoogleMap = supportMapFragment.awaitMap()
            onMapReady(googleMap)
        }

        setupCategorySpinner()
        setupDialogs()
        setupNavigation()
        setupPhoto()

    }

    private fun setupCategorySpinner() {

        // Create an ArrayAdapter using the string array and a default spinner layout
        val spinner =
            view?.findViewById<AutoCompleteTextView>(R.id.item_categories_spinner)
        ArrayAdapter.createFromResource(
            requireActivity(),
            R.array.item_categories,
            android.R.layout.simple_spinner_dropdown_item
        ).also { adapter ->
            // Apply the adapter to the spinner
            spinner?.setAdapter(adapter)
        }

        // Workaround for AutocompleteTexView
        spinner?.setText(viewModel.category.value, false)

    }

    private fun setupDialogs() {

        item_image.setOnClickListener {
            openPictureDialog()
        }

        item_date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            openDatePicker(year, month, day) { _, y, m, d -> viewModel.onDateSet(y, m, d) }
        }

        viewModel.snackbarTextEvent.observeEvent(viewLifecycleOwner) {
            view?.showSnackbar(getString(it), Snackbar.LENGTH_LONG)
        }

    }

    private fun setupPhoto() {

        viewModel.currentPhotoUri.observe(viewLifecycleOwner) { uri ->

            item_image.load(uri) {

                it.placeholder(R.mipmap.ic_launcher_round)
                    .apply(RequestOptions.circleCropTransform())

            }

        }

        viewModel.takePhotoEvent.observeEvent(viewLifecycleOwner) {
            takePicture(REQUEST_TAKE_PHOTO, it)
        }

    }

    private fun setupNavigation() {

        viewModel.logoutEvent.observeEvent(viewLifecycleOwner) {

            findNavController().popBackStack()
            openAlertDialog {

                setMessage(R.string.not_logged_in_error)
                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }

        }

        viewModel.onBackPressedEvent.observeEvent(viewLifecycleOwner) {
            if (it) {

                openAlertDialog {

                    setTitle(R.string.unsaved_title)
                        .setMessage(R.string.unsaved_message)
                        .setPositiveButton(android.R.string.ok) { dialog, _ ->
                            findNavController().popBackStack()
                            dialog.dismiss()
                        }
                        .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                            dialog.dismiss()
                        }

                }

            } else {
                findNavController().popBackStack()
            }
        }

        viewModel.saveEvent.observeEvent(viewLifecycleOwner) {
            if (it != args.item.id) {
                val action = ItemEditFragmentDirections
                    .openItemDetailsFragment(it)
                findNavController().navigate(action)
            } else {
                findNavController().popBackStack()
            }
        }

        requireActivity().onBackPressedDispatcher
            .addCallback(viewLifecycleOwner) { viewModel.onBackPressed() }

        viewModel.interestedUserEvent.observeEvent(viewLifecycleOwner) {

            openAlertDialog {

                val users = it.map { it.getFullName() ?: it.email }.toTypedArray()
                setItems(users) { dialog, which ->
                    viewModel.sellTo(it[which].uid)
                    dialog.dismiss()
                }

                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }

        }

    }

    private fun openPictureDialog() {
        val options = resources.getStringArray(R.array.picture_dialog_items)
        openPictureDialog(R.string.picture_dialog_title, options)
        { dialog, which ->
            when (which) {
                0 -> {
                    if (checkPermission(Manifest.permission.CAMERA, REQUEST_CAMERA_PERMISSION)) {
                        viewModel.takePhoto()
                    }
                }
                1 -> {
                    choosePictureFromGallery(REQUEST_CHOOSE_PHOTO)
                }
            }
            dialog.dismiss()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.getOrNull(0) == PackageManager.PERMISSION_GRANTED) {
                viewModel.takePhoto()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.edit_item_menu, menu)

        if (!viewModel.isNewItem) {

            menu.findItem(R.id.action_delete).isVisible = true

            val soldOutMenuItem = menu.findItem(R.id.action_sold_out)
            viewModel.boughtBy.observe(viewLifecycleOwner) {
                soldOutMenuItem.isVisible = it == null
            }

        }

    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.action_save -> {
                viewModel.save()
                return true
            }
            R.id.action_delete -> {
                viewModel.delete()
                return true
            }
            R.id.action_sold_out -> {
                viewModel.loadInterested()
                return true
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CHOOSE_PHOTO,
                REQUEST_TAKE_PHOTO -> {
                    viewModel.loadPhoto(data?.data)
                }
            }
        }
    }

    private suspend fun onMapReady(googleMap: GoogleMap) {

        googleMap.setOnMapClickListener {
            viewModel.newLocation(it)
        }

        viewModel.location.asFlow().collect { latLng ->

            with(googleMap) {
                clear()
                latLng?.also {
                    addMarker { position(it) }
                    animateCamera(CameraUpdateFactory.newLatLngZoom(it, MAP_LOCATION_ZOOM))
                }
            }

            item_location.text = try {
                @Suppress("BlockingMethodInNonBlockingContext")
                latLng?.getAddress(requireContext())?.locality
            } catch (e: Exception) {
                Timber.e(e)
                null
            }

        }
    }

}
