package it.polito.mad.madapplication.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.ui.MyBaseActivity
import it.polito.mad.madapplication.util.lazyFast
import it.polito.mad.madapplication.util.observeEvent
import javax.inject.Inject

const val EMAIL_KEY = "EMAIL_KEY"

class LoginActivity : MyBaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<LoginViewModel> { viewModelFactory }

    val loginComponent by lazyFast {
        (application as MadApplication)
            .appComponent.loginComponent().create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        loginComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel.loginEvent.observeEvent(this) {
            setResult(Activity.RESULT_OK, Intent().putExtra(EMAIL_KEY, it))
            finish()
        }

    }

}
