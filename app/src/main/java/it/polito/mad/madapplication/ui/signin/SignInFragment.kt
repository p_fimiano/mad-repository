package it.polito.mad.madapplication.ui.signin

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentSignInBinding
import it.polito.mad.madapplication.ui.login.LoginActivity
import it.polito.mad.madapplication.util.getStringOrNull
import it.polito.mad.madapplication.util.observeEvent
import it.polito.mad.madapplication.util.showSnackbar
import kotlinx.android.synthetic.main.fragment_sign_in.*
import javax.inject.Inject

class SignInFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<SignInViewModel> { viewModelFactory }

    private lateinit var viewDataBinding: FragmentSignInBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity() as LoginActivity)
            .loginComponent.signInComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_sign_in, container, false)
        viewDataBinding = FragmentSignInBinding.bind(root).also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.emailErrorEvent.observeEvent(viewLifecycleOwner) {
            email_layout.error = getStringOrNull(it)
        }

        viewModel.passwordErrorEvent.observeEvent(viewLifecycleOwner) {
            val errorText = getStringOrNull(it)
            password1_layout.error = errorText
            password2_layout.error = errorText
        }

        viewModel.signInErrorEvent.observeEvent(viewLifecycleOwner) {
            view.showSnackbar(getString(it), Snackbar.LENGTH_LONG)
        }

        viewModel.signInEvent.observeEvent(viewLifecycleOwner) {
            findNavController().popBackStack()
        }


    }

}
