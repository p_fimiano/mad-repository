package it.polito.mad.madapplication.data

import android.os.Parcelable
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.PropertyName
import it.polito.mad.madapplication.util.takeIfNotEmpty
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class User @JvmOverloads constructor(
    @DocumentId val uid: String = UUID.randomUUID().toString(),
    @get:PropertyName("email") val email: String = "",
    @get:PropertyName("name") val name: String? = null,
    @get:PropertyName("surname") val surname: String? = null,
    @get:PropertyName("nickname") val nickname: String? = null,
    @get:PropertyName("location") val location: Location? = null,
    @get:PropertyName("photo") val photo: FileRes? = null
) : Parcelable {

    fun getFullName(): String? {
        val name = name?.takeIfNotEmpty()
        val surname = surname?.takeIfNotEmpty()
        return name?.let { _name ->
            surname?.let { _surname -> "$_name $_surname" } ?: _name
        } ?: surname
    }

}