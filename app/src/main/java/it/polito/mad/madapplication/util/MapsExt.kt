@file:Suppress("BlockingMethodInNonBlockingContext")

package it.polito.mad.madapplication.util

import android.content.Context
import android.location.Geocoder
import com.google.android.gms.maps.model.LatLng
import it.polito.mad.madapplication.data.Location
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.*
import kotlin.coroutines.CoroutineContext

@Throws(
    IOException::class,
    IllegalArgumentException::class
)
suspend fun LatLng.getAddress(
    context: Context,
    locale: Locale = Locale.getDefault(),
    ioDispatcher: CoroutineContext = Dispatchers.IO
) = getAddress(latitude, longitude, context, locale, ioDispatcher)

@Throws(
    IOException::class,
    IllegalArgumentException::class
)
suspend fun getAddress(
    latitude: Double,
    longitude: Double,
    context: Context,
    locale: Locale = Locale.getDefault(),
    ioDispatcher: CoroutineContext = Dispatchers.IO
) = withContext(ioDispatcher) {
    val geocoder = Geocoder(context, locale)
    geocoder.getFromLocation(latitude, longitude, 1)
        .firstOrNull()
}

fun LatLng.toLocation(): Location {
    return Location(latitude, longitude)
}