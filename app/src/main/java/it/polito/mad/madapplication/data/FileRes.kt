package it.polito.mad.madapplication.data

import android.net.Uri
import android.os.Parcelable
import com.google.firebase.firestore.PropertyName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FileRes(
    @get:PropertyName("id") val id: String = "",
    @get:PropertyName("downloadUrl") val downloadUrl: String = ""
) : Parcelable {

    fun toUri() = try {
        Uri.parse(downloadUrl)
    } catch (e: Exception) {
        null
    }

}