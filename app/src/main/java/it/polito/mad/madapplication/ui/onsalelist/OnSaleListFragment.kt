package it.polito.mad.madapplication.ui.onsalelist

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.ui.ItemAdapter
import it.polito.mad.madapplication.util.goneIf
import it.polito.mad.madapplication.util.observeEvent
import kotlinx.android.synthetic.main.fragment_onsale_list.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import javax.inject.Inject

class OnSaleListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<OnSaleListViewModel> { viewModelFactory }

    private lateinit var dateFormat: DateFormat

    private lateinit var itemFilter: ItemFilter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.onSaleListComponent().create().inject(this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_onsale_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        dateFormat = SimpleDateFormat.getDateInstance()

        itemFilter =
            ItemFilter(ItemAdapter(
                dateFormat = dateFormat,
                openDetailsFragment = { v ->
                    val item = v.tag as Item
                    viewModel.openItem(item)
                }
            ))
        items_onsale_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = itemFilter.adapter
            setHasFixedSize(true)
        }

        viewModel.items.observe(viewLifecycleOwner) {
            empty_onsale_list_text_view.goneIf(it.isNotEmpty())
            itemFilter.submitList(it)
        }

        viewModel.openItemEvent.observeEvent(viewLifecycleOwner) {
            val action =
                OnSaleListFragmentDirections.openItemDetailsFragment(
                    it
                )
            findNavController().navigate(action)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_menu, menu)

        val menuItem = menu.findItem(R.id.action_search)
        val searchView = menuItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                itemFilter.perform(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                itemFilter.perform(newText)
                return true
            }

        })
    }

}