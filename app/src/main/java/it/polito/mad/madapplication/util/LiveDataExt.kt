package it.polito.mad.madapplication.util

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

@MainThread
fun <X, Y> LiveData<X>.switchMap(transform: (X) -> LiveData<Y>?): LiveData<Y?> {
    return MediatorLiveData<Y?>().also { result ->
        var mSource: LiveData<Y>? = null
        result.addSource(this) { x ->
            val newLiveData = transform.invoke(x)
            if (mSource === newLiveData) {
                return@addSource
            }
            if (mSource != null) {
                result.removeSource(mSource!!)
            }
            mSource = newLiveData
            if (mSource != null) {
                result.addSource(mSource!!) { y ->
                    result.value = y
                }
            } else {
                result.value = null
            }
        }
    }
}

@MainThread
fun <T1, T2, R> combineLatest(
    source1: LiveData<T1>,
    source2: LiveData<T2>,
    transform: (T1?, T2?) -> R
): LiveData<R> {
    return MediatorLiveData<R>().apply {
        addSource(source1) {
            value = transform.invoke(it, source2.value)
        }
        addSource(source2) {
            value = transform.invoke(source1.value, it)
        }
    }
}

@MainThread
fun <T1, T2, R> LiveData<T1>.combineLatestWith(
    source: LiveData<T2>,
    transform: (T1?, T2?) -> R
): LiveData<R> {
    return combineLatest(this, source, transform)
}