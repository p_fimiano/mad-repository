package it.polito.mad.madapplication.ui.itemsofinterestlist.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.itemsofinterestlist.ItemsOfInterestListViewModel

@Module
abstract class ItemsOfInterestModule {

    @Binds
    @IntoMap
    @ViewModelKey(ItemsOfInterestListViewModel::class)
    abstract fun bindViewModel(viewModel: ItemsOfInterestListViewModel): ViewModel

}