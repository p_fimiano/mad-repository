package it.polito.mad.madapplication.ui.onsalelist.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.onsalelist.OnSaleListViewModel

@Module
abstract class OnSaleListModule {

    @Binds
    @IntoMap
    @ViewModelKey(OnSaleListViewModel::class)
    abstract fun bindViewModel(viewModel: OnSaleListViewModel): ViewModel

}