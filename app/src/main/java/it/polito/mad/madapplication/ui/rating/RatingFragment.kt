package it.polito.mad.madapplication.ui.rating

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentRatingBinding
import it.polito.mad.madapplication.util.observeEvent
import it.polito.mad.madapplication.util.showSnackbar
import javax.inject.Inject

class RatingFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: RatingViewModel by viewModels { viewModelFactory }

    private val args: RatingFragmentArgs by navArgs()

    private lateinit var viewDataBinding: FragmentRatingBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.ratingComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_rating, container, false)
        viewDataBinding = FragmentRatingBinding.bind(root).also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
            it.itemId = args.itemId
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupNavigation()
        setupSnackbar()
    }

    private fun setupNavigation() {
        viewModel.submitEvent.observeEvent(viewLifecycleOwner) {
            findNavController().popBackStack()
        }
    }

    private fun setupSnackbar() {
        viewModel.errorEvent.observeEvent(viewLifecycleOwner) {
            view?.showSnackbar(getString(it), Snackbar.LENGTH_LONG)
        }
    }

}