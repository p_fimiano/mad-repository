package it.polito.mad.madapplication.ui.showprofile.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.showprofile.ShowProfileFragment

@Subcomponent
interface ShowProfileComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): ShowProfileComponent
    }

    fun inject(fragment: ShowProfileFragment)

}