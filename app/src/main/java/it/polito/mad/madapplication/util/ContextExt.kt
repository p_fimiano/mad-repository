package it.polito.mad.madapplication.util

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import java.io.File

private const val AUTHORITY = "it.polito.mad.madapplication.fileprovider"

fun Context.getUri(file: File): Uri {
    return FileProvider.getUriForFile(
        this,
        AUTHORITY,
        file
    )
}