package it.polito.mad.madapplication.ui.onsalelist

import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.ui.ItemBaseViewModel
import javax.inject.Inject

class OnSaleListViewModel @Inject constructor(
    itemRepository: ItemRepository
) : ItemBaseViewModel() {

    val items = itemRepository.getLiveData()

}