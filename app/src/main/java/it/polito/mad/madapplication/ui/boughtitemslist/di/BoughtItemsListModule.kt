package it.polito.mad.madapplication.ui.boughtitemslist.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.boughtitemslist.BoughtItemsListViewModel

@Module
abstract class BoughtItemsListModule {

    @Binds
    @IntoMap
    @ViewModelKey(BoughtItemsListViewModel::class)
    abstract fun bindViewModel(viewModel: BoughtItemsListViewModel): ViewModel

}