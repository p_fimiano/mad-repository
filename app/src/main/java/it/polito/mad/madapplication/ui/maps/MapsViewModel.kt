package it.polito.mad.madapplication.ui.maps

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.maps.model.DirectionsResult
import com.google.maps.model.DirectionsRoute
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.source.GeoRepository
import it.polito.mad.madapplication.util.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class MapsViewModel @Inject constructor(
    private val geoRepository: GeoRepository
) : ViewModel() {

    private val _route = MutableLiveData<DirectionsRoute>()
    val route: LiveData<DirectionsRoute> = _route

    private val _distance = MutableLiveData<Double>()
    val distance: LiveData<Double> = _distance

    private val _fromAddress = MutableLiveData<String>()
    val fromAddress: LiveData<String> = _fromAddress

    private val _toAddress = MutableLiveData<String>()
    val toAddress: LiveData<String> = _toAddress

    private val _errorEvent = MutableLiveData<Event<Unit>>()
    val errorEvent: LiveData<Event<Unit>> = _errorEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    private var isDataLoaded = false

    fun start(to: LatLng) {
        if (_dataLoading.value == true || isDataLoaded) {
            return
        }

        this.isDataLoaded = true

        _dataLoading.value = true
        viewModelScope.launch {

            val from = when (val result = geoRepository.getLastLocation()) {
                is Result.Success -> result.data
                else -> {
                    _errorEvent.value = Event(Unit)
                    return@launch
                }
            }

            val result = geoRepository.getDirections(from, to)
            if (result is Result.Success) {
                onDataLoaded(result.data)
            } else {
                _errorEvent.value = Event(Unit)
            }

            _dataLoading.value = false

        }
    }

    private fun onDataLoaded(result: DirectionsResult) {
        val route = result.routes.getOrNull(0) ?: return

        val leg = route.legs[0]
        _distance.value = leg.distance.inMeters / 1000.0
        _fromAddress.value = leg.startAddress
        _toAddress.value = leg.endAddress

        _route.value = route
    }

}