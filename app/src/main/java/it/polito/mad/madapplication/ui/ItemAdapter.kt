package it.polito.mad.madapplication.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.util.*
import kotlinx.android.synthetic.main.item_view.view.*
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import java.text.DateFormat
import java.util.*


class ItemDiffCallback : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem == newItem
    }
}

class ItemAdapter(
    private val dateFormat: DateFormat,
    private val openDetailsFragment: ((View) -> Unit),
    private val openEditFragment: ((View) -> Unit)? = null,
    private val openRatingFragment: ((View) -> Unit)? = null
) : ListAdapter<Item, ItemAdapter.ItemViewHolder>(
    ItemDiffCallback()
) {

    class ItemViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {

            fun create(itemView: View, viewType: Int): ItemViewHolder {
                val ctx = itemView.context
                (itemView.layoutParams as RecyclerView.LayoutParams).apply {

                    val margin = 16.dpToPx(ctx)
                    val marginTop = if (viewType == 1) margin else 4.dpToPx(ctx)
                    val marginBottom = if (viewType == 2) margin else 12.dpToPx(ctx)

                    setMargins(margin, marginTop, margin, marginBottom)
                }.also {
                    itemView.layoutParams = it
                }
                return ItemViewHolder(
                    itemView
                )
            }

        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 1
            itemCount - 1 -> 2
            else -> 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_view, parent, false).apply {
                item_edit_btn.setOnClickListener(openEditFragment)
                rating_btn.setOnClickListener(openRatingFragment)
                setOnClickListener(openDetailsFragment)
            }
        return ItemViewHolder.create(
            view,
            viewType
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.apply {

            // SetUp tags
            tag = item
            rating_btn.tag = item
            item_edit_btn.apply {
                invisibleIf(openEditFragment == null)
                tag = item
            }

            item_title_details.text = item.title?.takeIfNotEmpty()
            item_price_details.text = item.price?.toString()

            // TODO: Fix it, replace runBlocking with the right scope
            item_location_details.text = item.location?.let {
                runBlocking {
                    try {
                        it.toLatLng()
                            .getAddress(context)?.locality
                    } catch (e: Exception) {
                        Timber.e(e)
                        null
                    }
                }
            }

            item_date_details.text = item.expiryDate?.let {
                val date = Date(it)
                dateFormat.format(date)
            }

            val rating = item.rating?.vote ?: 0.0F
            ratingBar.rating = rating

            item_image_details.load(item.photo?.toUri()) {

                it.placeholder(R.mipmap.ic_launcher_round)
                    .apply(RequestOptions.circleCropTransform())

            }

        }

    }

}