package it.polito.mad.madapplication.ui.rating.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.rating.RatingViewModel

@Module
abstract class RatingModule {

    @Binds
    @IntoMap
    @ViewModelKey(RatingViewModel::class)
    abstract fun bindViewModel(viewModel: RatingViewModel): ViewModel

}