package it.polito.mad.madapplication.ui.editprofile

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.ktx.awaitMap
import com.google.maps.android.ktx.model.markerOptions
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentEditProfileBinding
import it.polito.mad.madapplication.ui.MySupportMapFragment
import it.polito.mad.madapplication.ui.login.EMAIL_KEY
import it.polito.mad.madapplication.ui.login.LoginActivity
import it.polito.mad.madapplication.ui.main.*
import it.polito.mad.madapplication.util.*
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class EditProfileFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: EditProfileViewModel.Factory

    private val viewModel by viewModels<EditProfileViewModel> {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return viewModelFactory.create(args.user) as T
            }
        }
    }

    private val args by navArgs<EditProfileFragmentArgs>()

    private lateinit var viewDataBinding: FragmentEditProfileBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.editProfileComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_edit_profile, container, false)
        viewDataBinding = FragmentEditProfileBinding.bind(root).also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMap()
        setupNavigation()
        setupEvents()
        setupPhoto()

    }

    private fun setupPhoto() {
        viewModel.currentPhoto.observe(viewLifecycleOwner) { path ->

            btn_camera.load(path) {

                it.placeholder(R.mipmap.ic_launcher_round)
                    .apply(RequestOptions.circleCropTransform())

            }

        }
    }

    private fun setupEvents() {
        viewModel.takePhotoEvent.observeEvent(viewLifecycleOwner) {
            takePicture(REQUEST_TAKE_PHOTO, it)
        }

        viewModel.snackbarErrorText.observeEvent(viewLifecycleOwner) {
            view?.showSnackbar(getString(it), Snackbar.LENGTH_LONG)
        }

        viewModel.emailErrorEvent.observeEvent(viewLifecycleOwner) {
            email_layout.error = getStringOrNull(it)
        }
    }

    private fun setupNavigation() {
        viewModel.saveEvent.observeEvent(viewLifecycleOwner) {
            findNavController().popBackStack()
        }

        viewModel.loginRequiredError.observeEvent(viewLifecycleOwner) {
            Intent(requireActivity(), LoginActivity::class.java).also { intent ->
                startActivityForResult(
                    intent,
                    REQUEST_LOGIN
                )
            }
        }

        viewModel.loginEvent.observeEvent(viewLifecycleOwner) {

            findNavController().popBackStack()
            openAlertDialog {

                setMessage(R.string.not_logged_in_error)
                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }

        }

        btn_camera.setOnClickListener {
            openPictureDialog()
        }
    }

    private fun setupMap() {
        val supportMapFragment = childFragmentManager
            .findFragmentById(R.id.map) as MySupportMapFragment
        supportMapFragment.listener = {
            scrollView.requestDisallowInterceptTouchEvent(true)
        }
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            val map: GoogleMap = supportMapFragment.awaitMap()
            onMapReady(map)
        }
    }

    private suspend fun onMapReady(googleMap: GoogleMap) {

        googleMap.setOnMapClickListener {
            viewModel.newLocation(it)
        }

        viewModel.location.asFlow().collect { location ->
            googleMap.clear()
            location?.also {
                @Suppress("BlockingMethodInNonBlockingContext")
                location_textView.text = it.getAddress(requireContext())?.locality
                googleMap.addMarker(markerOptions { position(it) })
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(it, MAP_LOCATION_ZOOM))
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.getOrNull(0) == PackageManager.PERMISSION_GRANTED) {
                viewModel.takePhoto()
            }
        }
    }

    private fun openPictureDialog() {
        val options = resources.getStringArray(R.array.picture_dialog_items)
        openPictureDialog(R.string.picture_dialog_title, options)
        { dialog, which ->
            when (which) {
                0 -> {
                    if (checkPermission(
                            Manifest.permission.CAMERA,
                            REQUEST_CAMERA_PERMISSION
                        )
                    ) {
                        viewModel.takePhoto()
                    }
                }
                1 -> {
                    choosePictureFromGallery(REQUEST_CHOOSE_PHOTO)
                }
            }
            dialog.dismiss()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_TAKE_PHOTO,
                REQUEST_CHOOSE_PHOTO -> {
                    viewModel.loadPhoto(data?.data)
                }
                REQUEST_LOGIN -> {
                    data?.getStringExtra(EMAIL_KEY)?.let { email ->
                        if (email == args.user.email) {
                            viewModel.save()
                        }
                    }
                }
            }
        }
    }

}
