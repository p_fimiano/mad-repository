@file:Suppress("unused")

package it.polito.mad.madapplication.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

private const val DEFAULT_TASK_TIMEOUT = 2L * 60L * 1000L
private const val DEFAULT_LD_TIMEOUT = 5L * 1000L

@Throws(Exception::class)
suspend fun <T> Task<T>.await(timeoutInMs: Long = DEFAULT_TASK_TIMEOUT) = withTimeout(timeoutInMs) {
    return@withTimeout suspendCancellableCoroutine<T> { cont ->
        addOnSuccessListener { cont.resume(it) }
        addOnFailureListener { cont.resumeWithException(it) }
    }
}

@Throws(Exception::class)
suspend fun UploadTask.await() = suspendCancellableCoroutine<UploadTask.TaskSnapshot> { cont ->
    cont.invokeOnCancellation { cancel() }
    addOnSuccessListener { cont.resume(it) }
    addOnFailureListener { cont.resumeWithException(it) }
}


@Throws(Exception::class)
fun <T : Any> Query.getLiveData(
    context: CoroutineContext,
    modelClass: Class<T>,
    timeoutInMs: Long = DEFAULT_LD_TIMEOUT
): LiveData<List<T>?> {
    return liveData(context, timeoutInMs) {
        val channel = Channel<QuerySnapshot?>(Channel.CONFLATED)
        val registration = addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                throw exception
            }
            channel.offer(snapshot)
        }
        try {
            for (snapshot in channel) {
                emit(snapshot?.mapNotNull { it.toObject(modelClass) })
            }
        } finally {
            registration.remove()
        }
    }
}

@Throws(Exception::class)
fun <T : Any> DocumentReference.getLiveData(
    context: CoroutineContext,
    modelClass: Class<T>,
    timeoutInMs: Long = DEFAULT_LD_TIMEOUT
): LiveData<T?> {
    return liveData(context, timeoutInMs) {
        val channel = Channel<DocumentSnapshot?>(Channel.CONFLATED)
        val registration = addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                throw exception
            }
            channel.offer(snapshot)
        }
        try {
            for (snapshot in channel) {
                if (snapshot != null && snapshot.exists()) {
                    emit(snapshot.toObject(modelClass))
                    continue
                }
                emit(null)
            }
        } finally {
            registration.remove()
        }
    }
}
