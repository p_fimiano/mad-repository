@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package it.polito.mad.madapplication.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

data class Event<out T : Any>(
    private val content: T
) {

    var hasBeenHandled = false
        private set

    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    fun peekContent(): T = content

}

class EventObserver<T : Any, R : Event<T>?>(
    private val onEventUnhandledContent: (T) -> Unit
) : Observer<R> {

    override fun onChanged(event: R) {
        event?.getContentIfNotHandled()?.also {
            onEventUnhandledContent(it)
        }
    }

}

fun <T : Any, R : Event<T>?> LiveData<R>.observeEvent(
    owner: LifecycleOwner,
    onChange: (T) -> Unit
) {
    observe(owner, EventObserver {
        onChange(it)
    })
}