package it.polito.mad.madapplication.util

import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.widget.DatePicker
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder


fun Activity.openDatePicker(
    year: Int, month: Int, day: Int,
    listener: (DatePicker, Int, Int, Int) -> Unit
) {
    DatePickerDialog(this, listener, year, month, day)
        .show()
}

fun Fragment.openDatePicker(
    year: Int, month: Int, day: Int,
    listener: (DatePicker, Int, Int, Int) -> Unit
) {
    requireActivity().openDatePicker(year, month, day, listener)
}

fun Activity.openPictureDialog(
    @StringRes title: Int,
    options: Array<String>,
    listener: (DialogInterface, Int) -> Unit
) {
    val builder = MaterialAlertDialogBuilder(this)
        .setTitle(title)
        .setItems(options, listener)
    builder.show()
}

fun Fragment.openPictureDialog(
    @StringRes title: Int,
    options: Array<String>,
    listener: (DialogInterface, Int) -> Unit
) {
    requireActivity().openPictureDialog(title, options, listener)
}

fun Activity.openAlertDialog(
    builder: MaterialAlertDialogBuilder.() -> Unit
) {
    MaterialAlertDialogBuilder(this).apply {
        builder.invoke(this)
        show()
    }
}

fun Fragment.openAlertDialog(
    builder: MaterialAlertDialogBuilder.() -> Unit
) {
    requireActivity().openAlertDialog(builder)
}
