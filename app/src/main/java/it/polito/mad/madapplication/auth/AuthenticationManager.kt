package it.polito.mad.madapplication.auth

import androidx.lifecycle.LiveData

interface AuthenticationManager {

    fun getCurrentLiveData(): LiveData<String?>

    suspend fun getCurrent(): String

    suspend fun deleteCurrent()

    suspend fun updateCurrentEmail(email: String)

    suspend fun login(email: String, password: String)

    suspend fun signIn(email: String, password: String): String

    suspend fun logout()

}