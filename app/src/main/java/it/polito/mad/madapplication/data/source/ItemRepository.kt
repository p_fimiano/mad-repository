package it.polito.mad.madapplication.data.source

import androidx.lifecycle.LiveData
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.Result

interface ItemRepository {

    fun getLiveData(): LiveData<List<Item>>

    fun getLiveDataByUser(userId: String): LiveData<List<Item>>

    fun getLiveDataById(id: String): LiveData<Item?>

    fun getLiveDataOfInterestByUser(userId: String): LiveData<List<Item>>

    fun getLiveDataOfBoughtByUser(userId: String): LiveData<List<Item>>

    suspend fun getById(id: String): Result<Item>

    suspend fun getAllByUser(userId: String): Result<List<Item>>

    suspend fun save(item: Item): Result<String>

    suspend fun deleteById(id: String): Result<Unit>

    suspend fun sellToById(id: String, uid: String): Result<Unit>

}