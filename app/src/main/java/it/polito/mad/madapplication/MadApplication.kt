package it.polito.mad.madapplication

import android.app.Application
import androidx.work.Configuration
import androidx.work.WorkerFactory
import it.polito.mad.madapplication.di.AppComponent
import it.polito.mad.madapplication.di.DaggerAppComponent
import timber.log.Timber
import javax.inject.Inject

open class MadApplication : Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: WorkerFactory

    open val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
    }

}
