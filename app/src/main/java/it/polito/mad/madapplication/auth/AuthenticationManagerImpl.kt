package it.polito.mad.madapplication.auth

import androidx.lifecycle.liveData
import com.google.firebase.auth.FirebaseAuth
import it.polito.mad.madapplication.util.await
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthenticationManagerImpl @Inject constructor(
    private val auth: FirebaseAuth,
    private val ioDispatcher: CoroutineDispatcher
) : AuthenticationManager {

    override fun getCurrentLiveData() = liveData(ioDispatcher) {
        Channel<FirebaseAuth>(Channel.CONFLATED).also { channel ->
            val listener: (FirebaseAuth) -> Unit = { auth ->
                channel.offer(auth)
            }
            auth.addAuthStateListener(listener)
            try {
                for (value in channel) {
                    emit(value.currentUser?.uid)
                }
            } finally {
                auth.removeAuthStateListener(listener)
            }
        }
    }

    @Throws(Exception::class)
    override suspend fun getCurrent(): String {
        return auth.currentUser?.uid
            ?: throw Exception("User not logged")
    }

    @Throws(Exception::class)
    override suspend fun deleteCurrent() {
        withContext(ioDispatcher) {
            val currentUser = auth.currentUser
                ?: throw Exception("User not logged")
            currentUser.delete().await()
        }
    }

    @Throws(Exception::class)
    override suspend fun updateCurrentEmail(email: String) {
        withContext(ioDispatcher) {
            val currentUser = auth.currentUser
                ?: throw Exception("User not logged")
            currentUser.updateEmail(email).await()
        }
    }

    @Throws(Exception::class)
    override suspend fun login(email: String, password: String) {
        withContext(ioDispatcher) {
            auth.signInWithEmailAndPassword(email, password)
                .await().user?.uid ?: throw Exception()
        }
    }

    @Throws(Exception::class)
    override suspend fun signIn(email: String, password: String) = withContext(ioDispatcher) {
        auth.createUserWithEmailAndPassword(email, password)
            .await().user?.uid ?: throw Exception()
    }

    override suspend fun logout() {
        auth.signOut()
    }

}