package it.polito.mad.madapplication.ui.login

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val email = MutableLiveData<String>()

    val password = MutableLiveData<String>()

    private val _emailErrorEvent = MutableLiveData<Event<Int>>()
    val emailErrorEvent: LiveData<Event<Int>> = _emailErrorEvent

    private val _passwordErrorEvent = MutableLiveData<Event<Int>>()
    val passwordErrorEvent: LiveData<Event<Int>> = _passwordErrorEvent

    private val _loginErrorEvent = MutableLiveData<Event<Int>>()
    val loginErrorEvent: LiveData<Event<Int>> = _loginErrorEvent

    private val _loginEvent = MutableLiveData<Event<String>>()
    val loginEvent: LiveData<Event<String>> = _loginEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    fun login() {
        if (_dataLoading.value == true) {
            return
        }

        _emailErrorEvent.value = Event(0)
        _passwordErrorEvent.value = Event(0)

        val currentEmail = email.value
        if (currentEmail == null || !Patterns.EMAIL_ADDRESS.matcher(currentEmail).matches()) {
            _emailErrorEvent.value =
                Event(R.string.email_error)
            return
        }

        val currentPassword = password.value
        password.value = null

        if (currentPassword.isNullOrBlank()) {
            _passwordErrorEvent.value =
                Event(R.string.password_error)
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val result = userRepository.login(currentEmail, currentPassword)
            if (result is Result.Success) {
                _loginEvent.value = Event(currentEmail)
                _dataLoading.value = false
            } else {
                onLoginError((result as? Result.Error)?.exception)
            }

        }

    }

    private fun onLoginError(exception: Exception?) {
        when (exception) {
            is FirebaseAuthInvalidUserException -> {
                _emailErrorEvent.value =
                    Event(R.string.user_error)
            }
            is FirebaseAuthInvalidCredentialsException -> {
                _passwordErrorEvent.value =
                    Event(R.string.password_error)
            }
        }
        _loginErrorEvent.value =
            Event(R.string.login_error)
        _dataLoading.value = false
    }

}