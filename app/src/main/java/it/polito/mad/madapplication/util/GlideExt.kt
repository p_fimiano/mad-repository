package it.polito.mad.madapplication.util

import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder

inline fun ImageView.load(uri: Uri?, builder: (RequestBuilder<Drawable>) -> Unit) {
    Glide.with(context)
        .load(uri)
        .also {
            builder.invoke(it)
        }
        .into(this)
}