package it.polito.mad.madapplication.ui.itemlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.ui.ItemBaseViewModel
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.switchMap
import kotlinx.coroutines.launch
import javax.inject.Inject

class ItemListViewModel @Inject constructor(
    private val userRepository: UserRepository,
    itemRepository: ItemRepository
) : ItemBaseViewModel() {

    val userItems = userRepository.getCurrentLiveData().switchMap { uid ->
        uid?.let { itemRepository.getLiveDataByUser(it.uid) }
    }

    private val _loginErrorEvent = MutableLiveData<Event<Unit>>()
    val loginErrorEvent: LiveData<Event<Unit>> = _loginErrorEvent

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    private val _editItemEvent = MutableLiveData<Event<Item>>()
    val editItemEvent: LiveData<Event<Item>> = _editItemEvent

    fun editItem(item: Item? = null) {
        if (_dataLoading.value == true) {
            return
        }

        if (item != null) {
            _editItemEvent.value = Event(item)
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {

            val result = userRepository.getCurrent()
            if (result is Result.Success) {
                _editItemEvent.value =
                    Event(Item(userId = result.data.uid))
            } else {
                _loginErrorEvent.value =
                    Event(Unit)
            }

            _dataLoading.value = false
        }
    }

}