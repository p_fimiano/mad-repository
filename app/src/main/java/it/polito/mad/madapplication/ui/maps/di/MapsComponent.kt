package it.polito.mad.madapplication.ui.maps.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.maps.MapsFragment

@Subcomponent(
    modules = [
        MapsModule::class
    ]
)
interface MapsComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): MapsComponent
    }

    fun inject(fragment: MapsFragment)

}