package it.polito.mad.madapplication.di

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.Multibinds
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Singleton
class DefaultWorkerFactory @Inject constructor(
    private val creators: Map<Class<out ListenableWorker>, @JvmSuppressWildcards Provider<AssistedWorkerFactory<*>>>
) : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {

        val workerClass = try {
            Class.forName(workerClassName)
        } catch (e: Throwable) {
            throw RuntimeException(e)
        }

        val creator: Provider<out AssistedWorkerFactory<*>> = creators[workerClass]
            ?: creators.asIterable().firstOrNull { workerClass.isAssignableFrom(it.key) }?.value
            ?: throw IllegalArgumentException("Unknown worker class: $workerClass")

        return try {
            creator.get().create(appContext, workerParameters)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

}

@Module
abstract class WorkerFactoryModule {

    @Binds
    abstract fun bindWorkerFactory(
        factory: DefaultWorkerFactory
    ): WorkerFactory

    @Multibinds
    @JvmSuppressWildcards
    abstract fun workerFactoryMap(): Map<Class<out ListenableWorker>, AssistedWorkerFactory<*>>

}

interface AssistedWorkerFactory<out T : ListenableWorker> {
    fun create(appContext: Context, params: WorkerParameters): T
}

@MapKey
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkerKey(val value: KClass<out ListenableWorker>)
