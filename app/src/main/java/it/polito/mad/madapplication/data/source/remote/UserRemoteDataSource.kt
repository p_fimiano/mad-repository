package it.polito.mad.madapplication.data.source.remote

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FieldPath
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.data.source.UserDataSource
import it.polito.mad.madapplication.di.AppModule.UsersCollection
import it.polito.mad.madapplication.util.await
import it.polito.mad.madapplication.util.getLiveData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRemoteDataSource @Inject constructor(
    @UsersCollection private val collectionReference: CollectionReference,
    private val ioDispatcher: CoroutineDispatcher
) : UserDataSource {

    @Throws(Exception::class)
    override fun getLiveDataById(uid: String) =
        collectionReference.document(uid).getLiveData(ioDispatcher, User::class.java)

    @Throws(Exception::class)
    override suspend fun getById(uid: String) = withContext(ioDispatcher) {
        collectionReference.document(uid).get()
            .await().let { snapshot ->
                if (snapshot.exists()) {
                    snapshot.toObject(User::class.java)
                } else {
                    null
                }
            }
    }

    @Throws(Exception::class)
    override suspend fun deleteById(uid: String) {
        withContext(ioDispatcher) {
            collectionReference
                .document(uid)
                .delete()
                .await()
        }
    }

    @Throws(Exception::class)
    override suspend fun save(user: User) {
        withContext(ioDispatcher) {
            collectionReference
                .document(user.uid)
                .set(user)
                .await()
        }
    }

    @Throws(Exception::class)
    @Suppress("UselessCallOnCollection")
    override suspend fun getAllByIds(uids: List<String>) = withContext(ioDispatcher) {
        collectionReference.whereIn(FieldPath.documentId(), uids).get()
            .await().mapNotNull { it.toObject(User::class.java) }
    }

}