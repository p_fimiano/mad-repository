package it.polito.mad.madapplication.ui.main.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.main.MainActivity

@Subcomponent(
    modules = [
        MainModule::class
    ]
)
interface MainComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): MainComponent
    }

    fun inject(activity: MainActivity)

}