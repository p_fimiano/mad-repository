package it.polito.mad.madapplication.data.source

import androidx.lifecycle.map
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.Result
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ItemRepositoryImpl @Inject constructor(
    private val remoteDataSource: ItemDataSource
) : ItemRepository {

    override fun getLiveData() = remoteDataSource.getLiveData().map {
        it ?: emptyList()
    }

    override fun getLiveDataByUser(userId: String) =
        remoteDataSource.getLiveDataByUser(userId).map {
            it ?: emptyList()
        }

    override fun getLiveDataById(id: String) = remoteDataSource.getLiveDataById(id)

    override fun getLiveDataOfInterestByUser(userId: String) =
        remoteDataSource.getLiveDataOfInterestByUser(userId).map {
            it ?: emptyList()
        }

    override fun getLiveDataOfBoughtByUser(userId: String) =
        remoteDataSource.getLiveDataOfBoughtByUser(userId).map {
            it ?: emptyList()
        }

    override suspend fun getById(id: String) = performOrError {
        remoteDataSource.getById(id)?.also {
            return@performOrError Result.Success(it)
        }
        Result.Error(Exception("Item not found"))
    }

    override suspend fun getAllByUser(userId: String) = performOrError {
        remoteDataSource.getAllByUser(userId)?.also {
            return@performOrError Result.Success(it)
        }
        Result.Error(Exception())
    }

    override suspend fun save(item: Item) = performOrError {
        remoteDataSource.save(item).let {
            Result.Success(it)
        }
    }

    override suspend fun deleteById(id: String) = performOrError {
        remoteDataSource.deleteById(id)
        Result.Success(Unit)
    }

    override suspend fun sellToById(id: String, uid: String)= performOrError {
        remoteDataSource.sellToById(id, uid)
        Result.Success(Unit)
    }

    private inline fun <T> performOrError(perform: () -> Result<T>): Result<T> {
        return try {
            perform.invoke()
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }

}