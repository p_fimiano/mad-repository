package it.polito.mad.madapplication.ui.login

import android.content.Context
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.FragmentLoginBinding
import it.polito.mad.madapplication.util.getStringOrNull
import it.polito.mad.madapplication.util.observeEvent
import it.polito.mad.madapplication.util.showSnackbar
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

class LoginFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by activityViewModels<LoginViewModel> { viewModelFactory }

    private lateinit var viewDataBinding: FragmentLoginBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity() as LoginActivity)
            .loginComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_login, container, false)
        viewDataBinding = FragmentLoginBinding.bind(root).also {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sign_in_textView.apply {
            movementMethod = LinkMovementMethod.getInstance()
            text = buildSpannedString {
                append(getString(R.string.no_account))
                append("\b")
                inSpans(
                    object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            findNavController()
                                .navigate(R.id.openSignInFragment)
                        }

                    }
                ) {
                    append(getString(R.string.sign_in))
                }
            }
        }

        viewModel.loginErrorEvent.observeEvent(viewLifecycleOwner) {
            view.showSnackbar(getString(it), Snackbar.LENGTH_LONG)
        }

        viewModel.emailErrorEvent.observeEvent(viewLifecycleOwner) {
            email_layout.error = getStringOrNull(it)
        }

        viewModel.passwordErrorEvent.observeEvent(viewLifecycleOwner) {
            password_layout.error = getStringOrNull(it)
        }

    }

}
