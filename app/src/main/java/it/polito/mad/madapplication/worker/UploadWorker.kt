package it.polito.mad.madapplication.worker

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import it.polito.mad.madapplication.data.FileRes
import it.polito.mad.madapplication.di.AssistedWorkerFactory
import it.polito.mad.madapplication.util.await
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject


class UploadWorker(
    private val firebaseFirestore: FirebaseFirestore,
    private val firebaseStorage: FirebaseStorage,
    private val ioDispatcher: CoroutineDispatcher,
    appContext: Context,
    params: WorkerParameters
) : CoroutineWorker(appContext, params) {

    override suspend fun doWork() = withContext(ioDispatcher) {
        try {

            val remoteId = inputData.getString(KEY_REMOTE_FILE_ID)
                ?: throw RuntimeException()

            val inputUri = inputData.getString(KEY_LOCAL_FILE_URI)
                ?: throw RuntimeException()

            val uri = Uri.parse(inputUri)

            val applicationContext = applicationContext
            val resolver = applicationContext.contentResolver

            val downloadUrl = upload(resolver, remoteId, uri)

            supervisorScope {
                launch { clean(resolver, uri) }
                launch { update(remoteId, downloadUrl) }
            }

            val outputData = workDataOf(
                KEY_LOCAL_FILE_URI to downloadUrl.toString()
            )

            Result.success(outputData)

        } catch (throwable: Throwable) {
            Timber.e(throwable)
            Result.failure()
        }

    }

    @Throws(IOException::class)
    private suspend fun upload(resolver: ContentResolver, remoteId: String, uri: Uri): Uri {

        val uploadRef = firebaseStorage.reference.child(remoteId)
        resolver.openInputStream(uri)?.use { uploadRef.putStream(it).await() }
            ?: throw RuntimeException("Unable to open input stream")

        return uploadRef.downloadUrl.await()
    }

    private suspend fun clean(resolver: ContentResolver, localeUri: Uri) {
        try {

            resolver.delete(localeUri, null, null)

            val remoteId = inputData.getString(KEY_REMOTE_CLEAN_FILE_ID) ?: return
            firebaseStorage.reference.child(remoteId).delete()
                .await()

        } catch (throwable: Throwable) {
            Timber.e(throwable)
        }
    }

    private suspend fun update(remoteId: String, downloadUrl: Uri) {
        try {

            val collectionId = inputData.getString(KEY_COLLECTION) ?: return
            val documentId = inputData.getString(KEY_DOCUMENT) ?: return
            val fieldId = inputData.getString(KEY_FIELD) ?: return

            val res = FileRes(remoteId, downloadUrl.toString())
            firebaseFirestore.collection(collectionId).document(documentId)
                .update(fieldId, res).await()

        } catch (e: Throwable) {
            Timber.e(e)
        }
    }

    companion object {

        private const val KEY_FIELD = "KEY_FIELD"
        private const val KEY_DOCUMENT = "KEY_DOCUMENT"
        private const val KEY_COLLECTION = "KEY_COLLECTION"
        private const val KEY_REMOTE_CLEAN_FILE_ID = "KEY_REMOTE_CLEAN_FILE_ID"
        private const val KEY_LOCAL_FILE_URI = "KEY_LOCAL_FILE_URI"
        private const val KEY_REMOTE_FILE_ID = "KEY_REMOTE_FILE_ID"

    }

    class Factory @Inject constructor(
        private val firebaseFirestore: FirebaseFirestore,
        private val firebaseStorage: FirebaseStorage,
        private val ioDispatcher: CoroutineDispatcher
    ) : AssistedWorkerFactory<UploadWorker> {
        override fun create(appContext: Context, params: WorkerParameters): UploadWorker {
            return UploadWorker(
                firebaseFirestore,
                firebaseStorage,
                ioDispatcher,
                appContext,
                params
            )
        }
    }

    class Builder(
        private val remoteFileId: String,
        private val localFileUri: String
    ) {

        private var documentId: String? = null
        private var collectionId: String? = null
        private var fieldId: String? = null

        private var cleanRemoteFileId: String? = null

        fun cleanRemoteFile(fileId: String?): Builder {
            cleanRemoteFileId = fileId
            return this
        }

        fun updateField(collectionId: String, documentId: String, fieldId: String): Builder {
            this.collectionId = collectionId
            this.documentId = documentId
            this.fieldId = fieldId
            return this
        }

        fun build(): Data {
            return workDataOf(
                KEY_REMOTE_FILE_ID to remoteFileId,
                KEY_LOCAL_FILE_URI to localFileUri,
                KEY_REMOTE_CLEAN_FILE_ID to cleanRemoteFileId,
                KEY_COLLECTION to collectionId,
                KEY_DOCUMENT to documentId,
                KEY_FIELD to fieldId
            )
        }

    }

}