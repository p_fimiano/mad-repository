package it.polito.mad.madapplication.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.data.source.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val user: LiveData<User?> = userRepository.getCurrentLiveData()

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    fun logout() {
        if (_dataLoading.value == true) {
            return
        }

        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.logout()
            _dataLoading.value = false
        }
    }

}