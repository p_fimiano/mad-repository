package it.polito.mad.madapplication.data

import android.os.Parcelable
import com.google.firebase.firestore.PropertyName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rating(
    @get:PropertyName("vote") val vote: Float = 0.0F,
    @get:PropertyName("comment") val comment: String? = null
) : Parcelable