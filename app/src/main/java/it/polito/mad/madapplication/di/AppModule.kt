package it.polito.mad.madapplication.di

import android.content.Context
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.firestoreSettings
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import com.google.maps.GeoApiContext
import dagger.Module
import dagger.Provides
import it.polito.mad.madapplication.R
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
object AppModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class ItemsCollection

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class UsersCollection

    @Provides
    @Singleton
    @JvmStatic
    fun provideIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Singleton
    @JvmStatic
    fun provideFirestore(): FirebaseFirestore {
        return Firebase.firestore.apply {
            firestoreSettings = firestoreSettings {
                cacheSizeBytes = FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED
                isPersistenceEnabled = true
            }
        }
    }

    @Provides
    @Singleton
    @JvmStatic
    @ItemsCollection
    fun provideItemsCollection(firestore: FirebaseFirestore): CollectionReference {
        return firestore.collection("items")
    }

    @Provides
    @Singleton
    @JvmStatic
    @UsersCollection
    fun provideUsersCollection(firestore: FirebaseFirestore): CollectionReference {
        return firestore.collection("users")
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideFirebaseStorage(): FirebaseStorage {
        return FirebaseStorage.getInstance()
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideFirebaseMessaging(): FirebaseMessaging {
        return FirebaseMessaging.getInstance()
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideGeoApiContext(context: Context): GeoApiContext {
        return GeoApiContext.Builder()
            .apiKey(context.getString(R.string.google_maps_key))
            .build()
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }

}