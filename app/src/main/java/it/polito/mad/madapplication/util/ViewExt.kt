@file:Suppress("unused")

package it.polito.mad.madapplication.util

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.goneIf(gone: Boolean) {
    if (gone) gone() else visible()
}

fun View.invisibleIf(invisible: Boolean) {
    if (invisible) invisible() else visible()
}

fun View.showSnackbar(text: String, duration: Int, builder: (Snackbar.() -> Unit)? = null) {
    Snackbar.make(this, text, duration).also { snackbar ->
        builder?.invoke(snackbar)
        snackbar.show()
    }
}