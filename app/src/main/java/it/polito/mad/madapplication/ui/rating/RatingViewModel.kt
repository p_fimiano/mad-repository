package it.polito.mad.madapplication.ui.rating

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Rating
import it.polito.mad.madapplication.data.Result
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class RatingViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val itemRepository: ItemRepository
) : ViewModel() {

    val vote = MutableLiveData<Float>()

    val comment = MutableLiveData<String>()

    private val _dataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean> = _dataLoading

    private val _errorEvent = MutableLiveData<Event<Int>>()
    val errorEvent: LiveData<Event<Int>> = _errorEvent

    private val _submitEvent = MutableLiveData<Event<Unit>>()
    val submitEvent: LiveData<Event<Unit>> = _submitEvent

    fun submit(itemId: String) {
        if (_dataLoading.value == true) {
            return
        }

        val currentVote = vote.value
        if (currentVote == null) {
            _errorEvent.value = Event(R.string.something_went_wrong)
            return
        }

        val currentComment = comment.value

        _dataLoading.value = true
        viewModelScope.launch {

            val currentItem = when (val result = itemRepository.getById(itemId)) {
                is Result.Success -> result.data
                else -> {
                    _errorEvent.value = Event(R.string.something_went_wrong)
                    return@launch
                }
            }

            val currentUid = when (val result = userRepository.getCurrent()) {
                is Result.Success -> result.data.uid
                else -> {
                    _errorEvent.value = Event(R.string.something_went_wrong)
                    return@launch
                }
            }

            if (currentItem.boughtBy != currentUid) {
                _errorEvent.value = Event(R.string.something_went_wrong)
                return@launch
            }

            val result =
                itemRepository.save(currentItem.copy(
                    rating = Rating(currentVote, currentComment)
                ))
            if (result is Result.Success) {
                _submitEvent.value = Event(Unit)
            } else {
                _errorEvent.value = Event(R.string.something_went_wrong)
            }

            _dataLoading.value = false
        }
    }


}