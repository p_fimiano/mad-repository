package it.polito.mad.madapplication.data

import android.os.Parcelable
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.PropertyName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Item @JvmOverloads constructor(
    @DocumentId val id: String? = null,
    @get:PropertyName("userId") val userId: String = "",
    @get:PropertyName("title") val title: String? = null,
    @get:PropertyName("description") val description: String? = null,
    @get:PropertyName("price") val price: Float? = null,
    @get:PropertyName("location") val location: Location? = null,
    @get:PropertyName("expiryDate") val expiryDate: Long? = null,
    @get:PropertyName("category") val category: String? = null,
    @get:PropertyName("photo") val photo: FileRes? = null,
    @get:PropertyName("userIds") val userIds: List<String> = emptyList(),
    @get:PropertyName("boughtBy") val boughtBy: String? = null,
    @get:PropertyName("rating") val rating: Rating? = null
) : Parcelable