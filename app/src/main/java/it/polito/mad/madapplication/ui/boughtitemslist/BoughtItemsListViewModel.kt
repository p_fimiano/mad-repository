package it.polito.mad.madapplication.ui.boughtitemslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.ui.ItemBaseViewModel
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.switchMap
import javax.inject.Inject

class BoughtItemsListViewModel @Inject constructor(
    userRepository: UserRepository,
    itemsRepository: ItemRepository
) : ItemBaseViewModel() {

    val boughtItems: LiveData<List<Item>?> =
        userRepository.getCurrentLiveData().switchMap { user ->
            user?.let { itemsRepository.getLiveDataOfBoughtByUser(it.uid) }
        }

    private val _loginErrorEvent = MutableLiveData<Event<Unit>>()
    val loginErrorEvent: LiveData<Event<Unit>> = _loginErrorEvent

}