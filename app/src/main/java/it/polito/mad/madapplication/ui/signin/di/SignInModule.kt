package it.polito.mad.madapplication.ui.signin.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.signin.SignInViewModel

@Module
abstract class SignInModule {

    @Binds
    @IntoMap
    @ViewModelKey(SignInViewModel::class)
    abstract fun bindViewModel(viewModel: SignInViewModel): ViewModel

}