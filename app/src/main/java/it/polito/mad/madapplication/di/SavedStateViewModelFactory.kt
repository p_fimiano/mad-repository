package it.polito.mad.madapplication.di

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.savedstate.SavedStateRegistryOwner
import dagger.Module
import dagger.Reusable
import dagger.multibindings.Multibinds
import javax.inject.Inject
import javax.inject.Provider

@Reusable
class ViewModelFactoryProvider @Inject constructor(
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<AssistedViewModelFactory<*>>>
) {

    fun factory(
        owner: SavedStateRegistryOwner,
        defaultArgs: Bundle?
    ): ViewModelProvider.Factory {
        return SavedStateViewModelFactory(creators, owner, defaultArgs)
    }

}

private class SavedStateViewModelFactory(
    private val creators: Map<Class<out ViewModel>, Provider<AssistedViewModelFactory<*>>>,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle?
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
    override fun <T : ViewModel?> create(
        modelKey: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {

        val creator: Provider<out AssistedViewModelFactory<*>> = creators[modelClass]
            ?: creators.asIterable().firstOrNull { modelClass.isAssignableFrom(it.key) }?.value
            ?: throw IllegalArgumentException("Unknown model class: $modelClass")

        @Suppress("UNCHECKED_CAST")
        return try {
            creator.get().create(handle) as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

}

@Module
abstract class ViewModelFactoryProviderModule {

    @Multibinds
    @JvmSuppressWildcards
    abstract fun viewModelFactoryMap(): Map<Class<out ViewModel>, AssistedViewModelFactory<*>>

}

interface AssistedViewModelFactory<out T : ViewModel> {
    fun create(handle: SavedStateHandle): T
}