package it.polito.mad.madapplication.ui.itemlist.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.itemlist.ItemListViewModel

@Module
abstract class ItemListModule {

    @Binds
    @IntoMap
    @ViewModelKey(ItemListViewModel::class)
    abstract fun bindViewModel(viewModel: ItemListViewModel): ViewModel

}