@file:Suppress("unused")

package it.polito.mad.madapplication.util

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

fun Activity.hasPermission(permission: String): Boolean {
    return ContextCompat.checkSelfPermission(this, permission) ==
            PackageManager.PERMISSION_GRANTED
}

fun Activity.hasPermissions(permissions: Array<String>): Boolean {
    for (permission in permissions) {
        if (!hasPermission(permission)) {
            return false
        }
    }
    return true
}

fun Activity.requestPermission(permission: String, requestCode: Int) {
    ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
}

