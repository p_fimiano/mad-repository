package it.polito.mad.madapplication.ui.itemsofinterestlist.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.itemsofinterestlist.ItemsOfInterestListFragment

@Subcomponent(
    modules = [
        ItemsOfInterestModule::class
    ]
)
interface ItemsOfInterestListComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): ItemsOfInterestListComponent
    }

    fun inject(fragment: ItemsOfInterestListFragment)

}