package it.polito.mad.madapplication.util

import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.provider.MediaStore
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Fragment.hasPermission(permission: String): Boolean {
    return requireActivity().hasPermission(permission)
}

fun Fragment.hasPermissions(permissions: Array<String>): Boolean {
    return requireActivity().hasPermissions(permissions)
}

fun Fragment.requestPermission(permission: String, requestCode: Int) {
    requestPermissions(arrayOf(permission), requestCode)
}

fun Fragment.checkPermission(permission: String, requestCode: Int): Boolean {
    if (!hasPermission(permission)) {
        requestPermission(permission, requestCode)
        return false
    }
    return true
}

fun Fragment.checkPermissions(permissions: Array<String>, requestCode: Int): Boolean {
    if (!hasPermissions(permissions)) {
        requestPermissions(permissions, requestCode)
        return false
    }
    return true
}

fun Fragment.choosePictureFromGallery(requestCode: Int) {
    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also {
        startActivityForResult(it, requestCode)
    }
}

fun Fragment.takePicture(requestCode: Int, imageUri: Uri) {
    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
        takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
            // Continue only if the File was successfully created
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            startActivityForResult(takePictureIntent, requestCode)
        }
    }
}

fun Fragment.getStringOrNull(@StringRes id: Int): String? {
    return try {
        getString(id)
    } catch (e: Resources.NotFoundException) {
        null
    }
}