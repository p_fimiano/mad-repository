package it.polito.mad.madapplication.ui.onsalelist.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.onsalelist.OnSaleListFragment

@Subcomponent(
    modules = [
        OnSaleListModule::class
    ]
)
interface OnSaleListComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): OnSaleListComponent
    }

    fun inject(fragment: OnSaleListFragment)

}