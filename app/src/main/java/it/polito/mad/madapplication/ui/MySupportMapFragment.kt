package it.polito.mad.madapplication.ui

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.gms.maps.SupportMapFragment


class MySupportMapFragment : SupportMapFragment() {

    var listener: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View? = super.onCreateView(inflater, parent, savedInstanceState)
        val frameLayout = TouchableWrapper(requireContext())
        frameLayout.setBackgroundColor(Color.TRANSPARENT)
        (view as ViewGroup).addView(
            frameLayout,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        return view
    }

    private inner class TouchableWrapper(context: Context) : FrameLayout(context) {
        override fun dispatchTouchEvent(event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> listener?.invoke()
                MotionEvent.ACTION_UP -> listener?.invoke()
            }
            return super.dispatchTouchEvent(event)
        }
    }

}