package it.polito.mad.madapplication.ui.boughtitemslist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.ui.ItemAdapter
import it.polito.mad.madapplication.ui.itemlist.ItemListFragmentDirections
import it.polito.mad.madapplication.util.goneIf
import it.polito.mad.madapplication.util.observeEvent
import it.polito.mad.madapplication.util.openAlertDialog
import kotlinx.android.synthetic.main.fragment_bought_items_list.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import javax.inject.Inject

class BoughtItemsListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: BoughtItemsListViewModel by viewModels { viewModelFactory }

    private lateinit var dateFormat: DateFormat

    private lateinit var itemAdapter: ItemAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MadApplication)
            .appComponent.boughtItemsListComponent().create().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bought_items_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dateFormat = SimpleDateFormat.getDateInstance()

        itemAdapter = ItemAdapter(
            dateFormat = dateFormat,
            openDetailsFragment = { v ->
                val item = v.tag as Item
                viewModel.openItem(item)
            },
            openRatingFragment = { v ->
                val item = v.tag as Item
                val action = BoughtItemsListFragmentDirections
                    .openRatingFragment(item.id!!)
                findNavController().navigate(action)
            }
        )
        items_bought_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = itemAdapter
            setHasFixedSize(true)
        }

        viewModel.boughtItems.observe(viewLifecycleOwner) {
            val list = it ?: emptyList()
            empty_list_bought_text_view.goneIf(list.isNotEmpty())
            itemAdapter.submitList(list)
        }

        viewModel.openItemEvent.observeEvent(viewLifecycleOwner) {
            val action = ItemListFragmentDirections
                .openItemDetailsFragment(it)
            findNavController().navigate(action)
        }

        viewModel.loginErrorEvent.observeEvent(viewLifecycleOwner) {
            openAlertDialog {

                setMessage(R.string.not_logged_in_error)
                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }

            }
        }

    }

}
