package it.polito.mad.madapplication.messaging

import com.google.firebase.messaging.FirebaseMessaging
import it.polito.mad.madapplication.util.await
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MessagingRepositoryImpl @Inject constructor(
    private val firebaseMessaging: FirebaseMessaging,
    private val ioDispatcher: CoroutineDispatcher
) : MessagingRepository {

    @Throws(Exception::class)
    override suspend fun subscribe(topic: String) = withContext<Unit>(ioDispatcher) {
        firebaseMessaging.subscribeToTopic(topic)
            .await()
    }

    @Throws(Exception::class)
    override suspend fun unsubscribe(topic: String) = withContext<Unit>(ioDispatcher) {
        firebaseMessaging.unsubscribeFromTopic(topic)
            .await()
    }

}