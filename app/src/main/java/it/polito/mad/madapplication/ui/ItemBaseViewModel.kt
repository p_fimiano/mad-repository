package it.polito.mad.madapplication.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.util.Event

open class ItemBaseViewModel : ViewModel() {

    private val _openItemEvent = MutableLiveData<Event<String>>()
    val openItemEvent: LiveData<Event<String>> = _openItemEvent

    fun openItem(item: Item) {
        _openItemEvent.value = Event(item.id!!)
    }

}