package it.polito.mad.madapplication.data.source.remote

import com.google.firebase.firestore.CollectionReference
import it.polito.mad.madapplication.data.Item
import it.polito.mad.madapplication.data.source.ItemDataSource
import it.polito.mad.madapplication.di.AppModule.ItemsCollection
import it.polito.mad.madapplication.util.await
import it.polito.mad.madapplication.util.getLiveData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ItemRemoteDataSource @Inject constructor(
    @ItemsCollection private val collectionReference: CollectionReference,
    private val ioDispatcher: CoroutineDispatcher
) : ItemDataSource {

    @Throws(Exception::class)
    override fun getLiveData() = collectionReference.getLiveData(ioDispatcher, Item::class.java)

    @Throws(Exception::class)
    override fun getLiveDataByUser(userId: String) =
        collectionReference.whereEqualTo("userId", userId)
            .getLiveData(ioDispatcher, Item::class.java)

    @Throws(Exception::class)
    override fun getLiveDataById(id: String) =
        collectionReference.document(id).getLiveData(ioDispatcher, Item::class.java)

    @Throws(Exception::class)
    override fun getLiveDataOfInterestByUser(userId: String) =
        collectionReference.whereArrayContains("userIds", userId)
            .getLiveData(ioDispatcher, Item::class.java)

    @Throws(Exception::class)
    override fun getLiveDataOfBoughtByUser(userId: String) =
        collectionReference.whereEqualTo("boughtBy", userId)
            .getLiveData(ioDispatcher, Item::class.java)

    @Throws(Exception::class)
    override suspend fun getById(id: String) = withContext(ioDispatcher) {
        collectionReference.document(id).get()
            .await().toObject(Item::class.java)
    }

    @Throws(Exception::class)
    override suspend fun getAllByUser(userId: String) = withContext(ioDispatcher) {
        collectionReference.whereEqualTo("userId", userId).get()
            .await().mapNotNull { it.toObject(Item::class.java) }
    }

    @Throws(Exception::class)
    override suspend fun save(item: Item) = withContext(ioDispatcher) {
        if (item.id == null) {
            collectionReference.add(item)
                .await().id
        } else {
            collectionReference.document(item.id).set(item)
                .await()
            item.id
        }
    }

    @Throws(Exception::class)
    override suspend fun deleteById(id: String) {
        withContext(ioDispatcher) {
            collectionReference.document(id).delete()
                .await()
        }
    }

    @Throws(Exception::class)
    override suspend fun sellToById(id: String, uid: String) {
        withContext(ioDispatcher) {
            collectionReference.document(id).update("boughtBy", uid)
                .await()
        }
    }

}