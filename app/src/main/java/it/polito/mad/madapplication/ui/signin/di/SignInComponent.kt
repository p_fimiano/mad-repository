package it.polito.mad.madapplication.ui.signin.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.signin.SignInFragment

@Subcomponent(
    modules = [
        SignInModule::class
    ]
)
interface SignInComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): SignInComponent

    }

    fun inject(fragment: SignInFragment)

}