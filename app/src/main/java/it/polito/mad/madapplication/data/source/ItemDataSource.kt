package it.polito.mad.madapplication.data.source

import androidx.lifecycle.LiveData
import it.polito.mad.madapplication.data.Item

interface ItemDataSource {

    fun getLiveData(): LiveData<List<Item>?>

    fun getLiveDataByUser(userId: String): LiveData<List<Item>?>

    fun getLiveDataById(id: String): LiveData<Item?>

    fun getLiveDataOfInterestByUser(userId: String): LiveData<List<Item>?>

    fun getLiveDataOfBoughtByUser(userId: String): LiveData<List<Item>?>

    suspend fun getById(id: String): Item?

    suspend fun getAllByUser(userId: String): List<Item>?

    suspend fun save(item: Item): String

    suspend fun deleteById(id: String)

    suspend fun sellToById(id: String, uid: String)

}