package it.polito.mad.madapplication.ui.showprofile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import it.polito.mad.madapplication.data.User
import it.polito.mad.madapplication.data.source.ItemRepository
import it.polito.mad.madapplication.data.source.UserRepository
import it.polito.mad.madapplication.util.Event
import it.polito.mad.madapplication.util.averageBy
import it.polito.mad.madapplication.util.switchMap
import javax.inject.Inject

class ShowProfileViewModel(
    userRepository: UserRepository,
    itemRepository: ItemRepository,
    private val userId: String?
) : ViewModel() {

    class Factory @Inject constructor(
        private val userRepository: UserRepository,
        private val itemRepository: ItemRepository
    ) {
        fun create(userId: String?): ShowProfileViewModel {
            return ShowProfileViewModel(userRepository, itemRepository, userId)
        }
    }

    private val _loggedUser: LiveData<User?> = userRepository.getCurrentLiveData()
    val user: LiveData<User?> = if (userId != null) {
        userRepository.getLiveDataById(userId)
    } else {
        _loggedUser
    }

    val isEditable: LiveData<Boolean> = _loggedUser.map {
        it != null && (userId == null || userId == it.uid)
    }

    val rating: LiveData<Float> = user.switchMap { user ->
        user?.let { itemRepository.getLiveDataByUser(it.uid) }
    }.map { items ->
        val list = items ?: emptyList()
        list.mapNotNull { it.rating }.averageBy { it.vote }
    }

    private val _editProfileEvent = MutableLiveData<Event<User>>()
    val editProfileEvent: LiveData<Event<User>> = _editProfileEvent

    private val _loginErrorEvent = MutableLiveData<Event<Unit>>()
    val loginErrorEvent: LiveData<Event<Unit>> = _loginErrorEvent

    fun editProfile() {
        if (isEditable.value != true) {
            _loginErrorEvent.value = Event(Unit)
            return
        }

        val currentUser = user.value ?: throw RuntimeException()
        _editProfileEvent.value = Event(currentUser)

    }

}