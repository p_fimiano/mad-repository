package it.polito.mad.madapplication.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.request.RequestOptions
import it.polito.mad.madapplication.MadApplication
import it.polito.mad.madapplication.R
import it.polito.mad.madapplication.databinding.ActivityMainBinding
import it.polito.mad.madapplication.ui.MyBaseActivity
import it.polito.mad.madapplication.ui.login.LoginActivity
import it.polito.mad.madapplication.ui.main.di.MainComponent
import it.polito.mad.madapplication.util.lazyFast
import it.polito.mad.madapplication.util.load
import it.polito.mad.madapplication.util.takeIfNotEmpty
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import javax.inject.Inject

class MainActivity : MyBaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<MainViewModel> { viewModelFactory }

    private lateinit var viewDataBinding: ActivityMainBinding

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navHeaderView: View

    @Suppress("MemberVisibilityCanBePrivate")
    val mainComponent: MainComponent by lazyFast {
        (application as MadApplication)
            .appComponent.mainComponent().create()
    }

    private lateinit var logoutMenuItem: MenuItem
    private lateinit var loginMenuItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {

        mainComponent.inject(this)

        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.viewModel = viewModel
        setSupportActionBar(toolbar)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration =
            AppBarConfiguration.Builder(
                R.id.nav_show_profile,
                R.id.nav_item_list,
                R.id.nav_item_on_sale,
                R.id.nav_items_of_interest,
                R.id.nav_items_bought
            )
                .setDrawerLayout(drawer_layout)
                .build()

        navHeaderView = nav_view.getHeaderView(0)

        val navController = findNavController(R.id.nav_host_fragment)

        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

        logoutMenuItem = nav_view.menu.findItem(R.id.nav_logout)
        loginMenuItem = nav_view.menu.findItem(R.id.nav_login)

        adjustStatusBar()

        loginMenuItem.setOnMenuItemClickListener {
            Intent(this, LoginActivity::class.java).also { intent ->
                startActivity(intent)
            }
            true
        }

        logoutMenuItem.setOnMenuItemClickListener {
            viewModel.logout()
            true
        }

        viewModel.user.observe(this) { user ->

            navHeaderView.user_image.load(user?.photo?.toUri()) {

                it.placeholder(R.mipmap.ic_launcher_round)
                    .apply(RequestOptions.circleCropTransform())

            }

            if (user != null) {

                navHeaderView.user_fullName.text = user.getFullName()
                navHeaderView.user_email.text = user.email.takeIfNotEmpty()

            } else {

                navHeaderView.user_fullName.text = getString(R.string.lbl_not_logged_in)
                navHeaderView.user_email.text = getString(R.string.lbl_please_log_in)
            }

            logoutMenuItem.isVisible = user != null
            loginMenuItem.isVisible = user == null

        }

    }

    private fun adjustStatusBar() {
        val resourceId = resources.getIdentifier(
            "status_bar_height", "dimen", "android"
        )
        if (resourceId > 0) {
            (navHeaderView.layoutParams as ViewGroup.MarginLayoutParams).apply {
                val height = resources.getDimensionPixelSize(resourceId)
                setMargins(leftMargin, topMargin + height, rightMargin, bottomMargin)
                navHeaderView.layoutParams = this
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}

const val REQUEST_CAMERA_PERMISSION = Activity.RESULT_FIRST_USER + 1
const val REQUEST_TAKE_PHOTO = Activity.RESULT_FIRST_USER + 2
const val REQUEST_CHOOSE_PHOTO = Activity.RESULT_FIRST_USER + 3
const val REQUEST_LOGIN = Activity.RESULT_FIRST_USER + 4
const val REQUEST_ACCESS_LOCATION_PERMISSION = Activity.RESULT_FIRST_USER + 5

const val MAP_LOCATION_ZOOM = 10F