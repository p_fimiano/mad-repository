package it.polito.mad.madapplication.data.source

import com.google.android.gms.maps.model.LatLng
import com.google.maps.model.DirectionsResult
import it.polito.mad.madapplication.data.Result

interface GeoRepository {

    suspend fun getLastLocation(): Result<LatLng>

    suspend fun getDirections(from: LatLng, to: LatLng): Result<DirectionsResult>

}