package it.polito.mad.madapplication.ui.maps.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.di.ViewModelKey
import it.polito.mad.madapplication.ui.maps.MapsViewModel

@Module
abstract class MapsModule {

    @Binds
    @IntoMap
    @ViewModelKey(MapsViewModel::class)
    abstract fun bindViewModel(viewModel: MapsViewModel): ViewModel

}