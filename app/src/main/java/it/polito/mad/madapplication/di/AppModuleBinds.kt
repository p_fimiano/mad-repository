package it.polito.mad.madapplication.di

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import it.polito.mad.madapplication.auth.AuthenticationManager
import it.polito.mad.madapplication.auth.AuthenticationManagerImpl
import it.polito.mad.madapplication.data.source.*
import it.polito.mad.madapplication.data.source.remote.ItemRemoteDataSource
import it.polito.mad.madapplication.data.source.remote.UserRemoteDataSource
import it.polito.mad.madapplication.messaging.MessagingRepository
import it.polito.mad.madapplication.messaging.MessagingRepositoryImpl
import it.polito.mad.madapplication.worker.UploadWorker

@Module
abstract class AppModuleBinds {

    @Binds
    abstract fun bindUserRepository(
        repo: UserRepositoryImpl
    ): UserRepository

    @Binds
    abstract fun bindItemRepository(
        repo: ItemRepositoryImpl
    ): ItemRepository

    @Binds
    abstract fun bindUserDataSource(
        source: UserRemoteDataSource
    ): UserDataSource

    @Binds
    abstract fun bindAuthManager(
        authManager: AuthenticationManagerImpl
    ): AuthenticationManager

    @Binds
    abstract fun bindItemDataSource(
        source: ItemRemoteDataSource
    ): ItemDataSource

    @Binds
    @IntoMap
    @WorkerKey(UploadWorker::class)
    abstract fun bindUploadWorkerFactory(
        factory: UploadWorker.Factory
    ): AssistedWorkerFactory<*>

    @Binds
    abstract fun bindMessagingRepository(
        repository: MessagingRepositoryImpl
    ): MessagingRepository

    @Binds
    abstract fun bindGeoRepository(
        repository: GeoRepositoryImpl
    ): GeoRepository

}