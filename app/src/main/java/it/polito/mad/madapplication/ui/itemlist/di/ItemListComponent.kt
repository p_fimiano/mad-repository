package it.polito.mad.madapplication.ui.itemlist.di

import dagger.Subcomponent
import it.polito.mad.madapplication.ui.edititem.ItemEditFragment
import it.polito.mad.madapplication.ui.itemlist.ItemListFragment
import it.polito.mad.madapplication.ui.onsalelist.OnSaleListFragment

@Subcomponent(
    modules = [
        ItemListModule::class
    ]
)
interface ItemListComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): ItemListComponent
    }

    fun inject(fragment: ItemListFragment)

    fun inject(fragment: OnSaleListFragment)

    fun inject(fragment: ItemEditFragment)

}